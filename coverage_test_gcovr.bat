@echo on  

pushd "%CI_PROJECT_DIR%"

gprbuild -d -P container_example\container_example.gpr --subdirs=gcov -cargs -g -fprofile-arcs -ftest-coverage -largs --coverage -fpreserve-control-flow  -fprofile-abs-path 

container_example\exe\gcov\main.exe

gcovr --config gcovr.cfg

7z.exe a -t7z -mx=5  "container_example\coverage_html.7z" "container_example\coverage_html"

popd 