

with Container_G.List_G.K_List_G;
with Rbc_Data_Types;

pragma Elaborate_All (Container_G.List_G.K_List_G);

package Interlocking_Mgr_Points_Container.List.K_List is new Interlocking_Mgr_Points_Container.List.K_List_G (
  Id_T   => Rbc_Data_Types.Identifier_T,
  Get_Id => Point.Get_Id,
  Set_Id => Point.Set_Id);
