

with Rm_Data_Types;
with Container_G;
with Point;
pragma Elaborate_All (Container_G);

package Interlocking_Mgr_Points_Container is new Container_G (
  Element_T      => Point.Point_T,
  Element_Acc_T  => Point.Point_Acc_T,
  Pos_Range_T    => Rm_Data_Types.Index_Point_T);
