
generic

  type Element_T is private;

  type Element_Acc_T is access all Element_T;

  type Pos_Range_T is range <>;
package Container_G is

  type Container_T is abstract tagged private;

  subtype Element_Ptr is Element_Acc_T;

  subtype Iterator_T is Pos_Range_T;

  subtype Index_Range_T is Pos_Range_T range 1 .. Pos_Range_T'Last;

  type Element_Set_T is array (Index_Range_T) of Element_Ptr;


  function Get
   (It   : in Iterator_T;
    Self : access Container_T)
    return Element_Ptr is abstract;


  function Get
   (It   : in Iterator_T;
    Self : in Container_T)
    return Element_T is abstract;
  procedure Next
   (It   : in out Iterator_T;
    Self : in Container_T) is abstract;

  function Done
   (It   : in Iterator_T;
    Self : in Container_T)
    return Boolean;

  type Proc_Access_T is access procedure (Elem  : access Element_T);
  type Proc_Idx_Access_T is access procedure (Elem  : access Element_T;
                                              Idx   : in Index_Range_T);
  procedure Execute (Self      : access Container_T;
                     This_Proc : in Proc_Access_T);
  procedure Execute (Self      : access Container_T;
                     This_Proc : in Proc_Idx_Access_T);

  function Getstatus
   (Self : in Container_T;
    Pos  : in Index_Range_T)
    return Boolean is abstract;


  generic

    type Reference_T is private;

    with function Is_Selected
     (Elem : access Element_T;
      Ref  : in Reference_T)
      return Boolean;
  function Selected_Subset
   (Self : access Container_T'Class;
    Ref  : in Reference_T)
    return Element_Set_T;


  generic

    type Reference_T is private;

    with function Is_Selected
     (Elem : access Element_T;
      Ref  : in Reference_T)
      return Boolean;
  function Selected_Element
   (Self : access Container_T'Class;
    Ref  : in Reference_T)
    return Element_Ptr;

  function Getsize
   (Self : in Container_T)
    return Pos_Range_T;

private

  Full  : constant Pos_Range_T := Pos_Range_T'Last;
  Empty : constant Pos_Range_T := Pos_Range_T'First;

  type Element_Array_T is array (Index_Range_T) of aliased Element_T;

  type Container_T is abstract tagged
    record
      T_Element : Element_Array_T;
      Size      : Pos_Range_T := Empty;
    end record;


end Container_G;
