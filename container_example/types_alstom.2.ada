

with Unchecked_Conversion;
with Ada.Numerics.Generic_Elementary_Functions;
with Interfaces;
with System;

package body Types_Alstom is

  function Image
   (From : in Long_64_T)
    return String renames Long_64_T'Image;

  package Math is new Ada.Numerics.Generic_Elementary_Functions (Float);

  package body Counter is

    Process_Is_Active : Boolean_T      := False;
    Logging_Routine   : Logging_Access := null;
    Process_Is_Not_Active : exception;

    subtype Operation_Cnt_T is Integer;
    subtype Operation_Time_T is Integer;

    Operation_Cnt     : array (Operation_T) of Operation_Cnt_T := (others => 0);
    Max_Operation_Cnt : array (Operation_T) of Operation_Cnt_T := (others => 0);
    Min_Operation_Cnt : array (Operation_T) of Operation_Cnt_T := (others => 0);
    Sum_Operation_Cnt : array (Operation_T) of Operation_Cnt_T := (others => 0);

  
    Time_Coef : constant array (Operation_T) of Operation_Time_T :=
     (Float_Add           => 23,
      Float_Sub           => 26,
      Float_Mul           => 23,
      Float_Div           => 30,
      Float_Sqr           => 110,
      Float_Exp           => 69,
      Float_Ln            => 59,
      Float_Abs_1         => 2,
      Float_Add_1         => 1,
      Float_Sub_1         => 2,
      Float_Gt            => 2,
      Float_Lt            => 2,
      Float_Gte           => 2,
      Float_Lte           => 2,
      Float_Eq            => 2,
      Float_Conv_From_Int => 5,
      Float_Conv_To_Int   => 5,
      Float_Conv_From_Fix => 10,
      Float_Conv_To_Fix   => 10,
      Float_Total_Time    => 0,

      Int64_Add           => 1,
      Int64_Sub           => 1,
      Int64_Mul           => 1,
      Int64_Div           => 45,
      Int64_Mod           => 1,
      Int64_Abs_1         => 1,
      Int64_Add_1         => 1,
      Int64_Sub_1         => 1,
      Int64_Gt            => 1,
      Int64_Lt            => 1,
      Int64_Gte           => 1,
      Int64_Lte           => 1,
      Int64_Conv_To_Int   => 1,
      Int64_Conv_From_Int => 1,
      Int64_Conv_To_Real  => 1,
      Int64_Total_Time    => 0,

      Logic_Not        => 1,
      Logic_Or         => 1,
      Logic_And        => 1,
      Logic_Xor        => 1,
      Logic_Total_Time => 0,

      Shift_Left       => 1,
      Shift_Right      => 1,
      Shift_Total_Time => 1,
      Total_Time       => 0);

    Cycle_Cnt : Integer := 0;

  
    procedure Init (Logging_Proc : in Logging_Access) is
    begin
      Process_Is_Active := True;
      Logging_Routine   := Logging_Proc;
    end Init;

    procedure Increment_Operation_Cnt (Op : in Operation_T) is
    begin
      if Process_Is_Active
      then
        Operation_Cnt (Op) := Operation_Cnt (Op) + 1;
      end if;
    exception
      when Constraint_Error =>
        null;
    end Increment_Operation_Cnt;

    procedure Compute_Operation_Time is
    begin
    
      Operation_Cnt (Float_Total_Time) := 0;
      for I in Float_Operation_T
      loop
        if I /= Float_Total_Time
        then
          begin
            Operation_Cnt (Float_Total_Time) := Operation_Cnt (Float_Total_Time) + (Time_Coef (I) * Operation_Cnt (I));
          exception
            when Constraint_Error =>
              Operation_Cnt (Float_Total_Time) := Operation_Cnt_T'Last;
          end;
        end if;
      end loop;
    
      Operation_Cnt (Int64_Total_Time) := 0;
      for I in Int64_Operation_T
      loop
        if I /= Int64_Total_Time
        then
          begin
            Operation_Cnt (Int64_Total_Time) := Operation_Cnt (Int64_Total_Time) + (Time_Coef (I) * Operation_Cnt (I));
          exception
            when Constraint_Error =>
              Operation_Cnt (Int64_Total_Time) := Operation_Cnt_T'Last;
          end;
        end if;
      end loop;
    
      Operation_Cnt (Logic_Total_Time) := 0;
      for I in Logic_Operation_T
      loop
        if I /= Logic_Total_Time
        then
          begin
            Operation_Cnt (Logic_Total_Time) := Operation_Cnt (Logic_Total_Time) + (Time_Coef (I) * Operation_Cnt (I));
          exception
            when Constraint_Error =>
              Operation_Cnt (Logic_Total_Time) := Operation_Cnt_T'Last;
          end;
        end if;
      end loop;
    
      Operation_Cnt (Shift_Total_Time) := 0;
      for I in Shift_Operation_T
      loop
        if I /= Shift_Total_Time
        then
          begin
            Operation_Cnt (Shift_Total_Time) := Operation_Cnt (Shift_Total_Time) + (Time_Coef (I) * Operation_Cnt (I));
          exception
            when Constraint_Error =>
              Operation_Cnt (Shift_Total_Time) := Operation_Cnt_T'Last;
          end;
        end if;
      end loop;
      begin
        Operation_Cnt (Total_Time) :=
         Operation_Cnt (Float_Total_Time) + Operation_Cnt (Int64_Total_Time) + Operation_Cnt (Logic_Total_Time) + Operation_Cnt (Shift_Total_Time);
      exception
        when Constraint_Error =>
          Operation_Cnt (Total_Time) := Operation_Cnt_T'Last;
      end;
    end Compute_Operation_Time;

    procedure Display_Operation_Count
     (For_Operation : in Operation_Selector_T := All_Operation_C;
      Cont_Kind     : in Cont_Kind_T          := All_Kind) is
      New_Line : constant String := "";
      procedure Print (Title : in String) is
        Underline : constant String (Title'Range) := (others => '=');
      begin
        Logging_Routine (Title);
        Logging_Routine (Underline);
      end Print;
    begin
      if not Process_Is_Active
      then
        raise Process_Is_Not_Active;
      end if;
      Compute_Operation_Time;
      if Cont_Kind = All_Kind or Cont_Kind = Max
      then
        Print (Title => "Max operation cont");
        for I in Operation_T
        loop
          if For_Operation (I)
          then
            Logging_Routine (Operation_T'Image (I) & " => " & Operation_Cnt_T'Image (Max_Operation_Cnt (I)));
          end if;
        end loop;
        Logging_Routine (New_Line);
      end if;

      if Cont_Kind = All_Kind or Cont_Kind = Min
      then
        Print (Title => "Min operation cont");
        for I in Operation_T
        loop
          if For_Operation (I)
          then
            Logging_Routine (Operation_T'Image (I) & " => " & Operation_Cnt_T'Image (Min_Operation_Cnt (I)));
          end if;
        end loop;
        Logging_Routine (New_Line);
      end if;

      if Cont_Kind = All_Kind or Cont_Kind = Sum
      then
        Print (Title => "Sum operation cont");
        for I in Operation_T
        loop
          if For_Operation (I)
          then
            Logging_Routine (Operation_T'Image (I) & " => " & Operation_Cnt_T'Image (Sum_Operation_Cnt (I)));
          end if;
        end loop;
        Logging_Routine (New_Line);
      end if;

      if Cont_Kind = All_Kind or Cont_Kind = Mean
      then
        Print (Title => "Mean operation cont (" & Operation_Cnt_T'Image (Cycle_Cnt) & " cycles)");
        for I in Operation_T
        loop
          Logging_Routine (Operation_T'Image (I) & " => " & Operation_Cnt_T'Image (Sum_Operation_Cnt (I) / Cycle_Cnt));
        end loop;
        Logging_Routine (New_Line);
      end if;

      if Cont_Kind = All_Kind or Cont_Kind = Last_Cycle
      then
        Print (Title => "Last cycle operation cont");
        for I in Operation_T
        loop
          if For_Operation (I)
          then
            Logging_Routine (Operation_T'Image (I) & " => " & Operation_Cnt_T'Image (Operation_Cnt (I)));
          end if;
        end loop;
        Logging_Routine (New_Line);
      end if;
    end Display_Operation_Count;

    procedure Reset_Operation_Count is
    begin
      if not Process_Is_Active
      then
        raise Process_Is_Not_Active;
      end if;
      Compute_Operation_Time;
      for I in Operation_T
      loop
        begin
          Sum_Operation_Cnt (I) := Sum_Operation_Cnt (I) + Operation_Cnt (I);
        exception
          when Constraint_Error =>
            Sum_Operation_Cnt (I) := Operation_Cnt_T'Last;
        end;
        Max_Operation_Cnt (I) := Operation_Cnt_T'Max (Max_Operation_Cnt (I), Operation_Cnt (I));
        Min_Operation_Cnt (I) := Operation_Cnt_T'Min (Min_Operation_Cnt (I), Operation_Cnt (I));

        Operation_Cnt (I) := 0;
      end loop;
      begin
        Cycle_Cnt := Cycle_Cnt + 1;
      exception
        when Constraint_Error =>
          Cycle_Cnt := Operation_Cnt_T'Last;
      end;
    end Reset_Operation_Count;

  end Counter;  type B8_T is array (0 .. 7) of Boolean_T;
  pragma Pack (B8_T);
  type B16_T is array (0 .. 15) of Boolean_T;
  pragma Pack (B16_T);
  type B32_T is array (0 .. 31) of Boolean_T;
  pragma Pack (B32_T);
  type Bit_Array_32_T is array (0 .. 31) of Bit_T;
  pragma Pack (Bit_Array_32_T);
  for Bit_Array_32_T'Size use 32;
  type Unsigned_Sw_T is record
    Msb : Unsigned_Byte_T;
    Lsb : Unsigned_Byte_T;
  end record;

  for Unsigned_Sw_T'Alignment use 2;
  for Unsigned_Sw_T use record
    Lsb at 0 range 0 .. 7;
    Msb at 1 range 0 .. 7;
  end record;

  for Unsigned_Sw_T'Size use Unsigned_Word_T'Size;
  type Unchecked_Sw_T is record
    Msb : Unchecked_Byte_T;
    Lsb : Unchecked_Byte_T;
  end record;

  for Unchecked_Sw_T'Alignment use 2;
  for Unchecked_Sw_T use record
    Lsb at 0 range 0 .. 7;
    Msb at 1 range 0 .. 7;
  end record;

  for Unchecked_Sw_T'Size use Unchecked_Word_T'Size;
  type Unsigned_Sl_T is record
    Msw : Unsigned_Word_T;
    Lsw : Unsigned_Word_T;
  end record;

  for Unsigned_Sl_T'Alignment use 2;
  for Unsigned_Sl_T use record
    Lsw at 0 range 0 .. 15;
    Msw at 2 range 0 .. 15;
  end record;

  for Unsigned_Sl_T'Size use Unsigned_Long_T'Size;
  type Unchecked_Sl_T is record
    Msw : Unchecked_Word_T;
    Lsw : Unchecked_Word_T;
  end record;

  for Unchecked_Sl_T'Alignment use 2;
  for Unchecked_Sl_T use record
    Lsw at 0 range 0 .. 15;
    Msw at 2 range 0 .. 15;
  end record;

  for Unchecked_Sl_T'Size use Unchecked_Long_T'Size;

  function To_Long_Long_Integer is new Unchecked_Conversion (Target_Long_64_T, Long_64_T);
  function To_Target_Long_64_T is new Unchecked_Conversion (Long_64_T, Target_Long_64_T);
  function B8 is new Unchecked_Conversion (Source => Unsigned_Byte_T, Target => B8_T);
  function Ub is new Unchecked_Conversion (Source => B8_T, Target => Unsigned_Byte_T);
  function B16 is new Unchecked_Conversion (Source => Unsigned_Word_T, Target => B16_T);
  function Uw is new Unchecked_Conversion (Source => B16_T, Target => Unsigned_Word_T);
  function Sw is new Unchecked_Conversion (Source => Unsigned_Word_T, Target => Unsigned_Sw_T);
  function Uw is new Unchecked_Conversion (Source => Unsigned_Sw_T, Target => Unsigned_Word_T);
  function Sw is new Unchecked_Conversion (Source => Unchecked_Word_T, Target => Unchecked_Sw_T);
  function Uw is new Unchecked_Conversion (Source => Unchecked_Sw_T, Target => Unchecked_Word_T);
  function B32 is new Unchecked_Conversion (Source => Unsigned_Long_T, Target => B32_T);
  function Ul is new Unchecked_Conversion (Source => B32_T, Target => Unsigned_Long_T);
  function Sl is new Unchecked_Conversion (Source => Unsigned_Long_T, Target => Unsigned_Sl_T);
  function B32 is new Unchecked_Conversion (Source => Unchecked_Long_T, Target => B32_T);
  function Sl is new Unchecked_Conversion (Source => Unchecked_Long_T, Target => Unchecked_Sl_T);
  function Ul is new Unchecked_Conversion (Source => Unchecked_Sl_T, Target => Unchecked_Long_T);
  function To_Bit_Array_32 is new Unchecked_Conversion (Source => Unchecked_Long_T, Target => Bit_Array_32_T);
  function To_Long is new Unchecked_Conversion (Source => Bit_Array_32_T, Target => Long_T);
  function Conv is new Unchecked_Conversion (Source => Unsigned_Byte_T, Target => Byte_In_Bits_T);
  function Conv is new Unchecked_Conversion (Source => Byte_In_Bits_T, Target => Unsigned_Byte_T);  function To_Unsigned_Long is new Unchecked_Conversion (Source => Bit_Array_32_T, Target => Unsigned_Long_T);





  procedure Enable_Real_Error (Enable : Boolean_T) is
  begin
    Error_Handling_Enabled := Enable;
  end Enable_Real_Error;

  function Is_Enable_Real_Error return Boolean_T is
  begin
    return Error_Handling_Enabled;
  end Is_Enable_Real_Error;

  function "not"
   (Op : in Unsigned_Byte_T)
    return Unsigned_Byte_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_Not);
    return Ub (not B8 (Op));
  end "not";
  function "or"
   (G, D : in Unsigned_Byte_T)
    return Unsigned_Byte_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_Or);
    return Ub (B8 (G) or B8 (D));
  end "or";
  function "and"
   (G, D : in Unsigned_Byte_T)
    return Unsigned_Byte_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_And);
    return Ub (B8 (G) and B8 (D));
  end "and";
  function "xor"
   (G, D : in Unsigned_Byte_T)
    return Unsigned_Byte_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_Xor);
    return Ub (B8 (G) xor B8 (D));
  end "xor";  function "not"
   (Op : in Unsigned_Word_T)
    return Unsigned_Word_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_Not);
    return Uw (not B16 (Op));
  end "not";
  function "or"
   (G, D : in Unsigned_Word_T)
    return Unsigned_Word_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_Or);
    return Uw (B16 (G) or B16 (D));
  end "or";
  function "and"
   (G, D : in Unsigned_Word_T)
    return Unsigned_Word_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_And);
    return Uw (B16 (G) and B16 (D));
  end "and";
  function "xor"
   (G, D : in Unsigned_Word_T)
    return Unsigned_Word_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_Xor);
    return Uw (B16 (G) xor B16 (D));
  end "xor";

  function "not"
   (Op : in Unsigned_Long_T)
    return Unsigned_Long_T is
  
  
    Mask_Unsigned_Long : constant Unchecked_Long_T := 16#7FFF_FFFF#;
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_Not);
    return Ul ((not (B32 (Op))) and (B32 (Mask_Unsigned_Long)));
  end "not";
  function "or"
   (G, D : in Unsigned_Long_T)
    return Unsigned_Long_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_Or);
    return Ul (B32 (G) or B32 (D));
  end "or";
  function "and"
   (G, D : in Unsigned_Long_T)
    return Unsigned_Long_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_And);
    return Ul (B32 (G) and B32 (D));
  end "and";
  function "xor"
   (G, D : in Unsigned_Long_T)
    return Unsigned_Long_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Logic_Xor);
    return Ul (B32 (G) xor B32 (D));
  end "xor";
  function ">="
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Gte);
    return Long_Long_Integer (Left) >= Long_Long_Integer (Right);
  end ">=";
  function ">"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Gt);
    return Long_Long_Integer (Left) > Long_Long_Integer (Right);
  end ">";
  function "<="
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Lte);
    return Long_Long_Integer (Left) <= Long_Long_Integer (Right);
  end "<=";
  function "<"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Lt);
    return Long_Long_Integer (Left) < Long_Long_Integer (Right);
  end "<";

  function "<"
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Lt);
    return Long_Long_Integer (Left) < Long_Long_Integer (To_Long_64_T (Right));
  end "<";
  function ">="
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T is
  begin
    return (not (Left < Right));
  end ">=";
  function ">"
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T is
  begin
    return (Right < Left);
  end ">";
  function "<="
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T is
  begin
    return (not (Right < Left));
  end "<=";
  function "="
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T is
  begin
    return (Left = To_Long_64 (Right));
  end "=";

  function "<"
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Lt);
    return Long_Long_Integer (Left) < Long_Long_Integer (Right);
  end "<";
  function ">="
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T is
  begin
    return (not (Left < Right));
  end ">=";
  function ">"
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T is
  begin
    return (Right < Left);
  end ">";
  function "<="
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T is
  begin
    return (not (Right < Left));
  end "<=";
  function "="
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T is
  begin
    return (Right = To_Long_64 (Left));
  end "=";

  function "="
   (Left, Right : in Real_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Eq);
    return Left.Value = Right.Value;
  end "=";
  function ">"
   (Left, Right : in Real_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Gt);
    return Left.Value > Right.Value;
  end ">";
  function "<"
   (Left, Right : in Real_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Lt);
    return Left.Value < Right.Value;
  end "<";
  function ">="
   (Left, Right : in Real_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Gte);
    return Left.Value >= Right.Value;
  end ">=";
  function "<="
   (Left, Right : in Real_T)
    return Boolean_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Lte);
    return Left.Value <= Right.Value;
  end "<=";

  function "+"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Long_64_T is
    Comp   : Long_Long_Integer;
    Result : Long_64_T;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Add);
    Comp   := Long_Long_Integer (Left) + Long_Long_Integer (Right);
    Result := Long_64_T (Comp);
    return Result;
  end "+";
  function "-"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Long_64_T is
    Comp   : Long_Long_Integer;
    Result : Long_64_T;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Sub);
    Comp   := Long_Long_Integer (Left) - Long_Long_Integer (Right);
    Result := Long_64_T (Comp);
    return Result;
  end "-";
  function "+"
   (Left  : Long_64_T;
    Right : Long_T)
    return Long_64_T is
    Result : Long_64_T;
  begin
    Result := Left + To_Long_64_T (Right);
    return Result;
  end "+";
  function "+"
   (Left  : Long_T;
    Right : Long_64_T)
    return Long_64_T is
    Result : Long_64_T;
  begin
    Result := To_Long_64_T (Left) + Right;
    return Result;
  end "+";
  function "-"
   (Left  : Long_64_T;
    Right : Long_T)
    return Long_64_T is
    Result : Long_64_T;
  begin
    Result := Left - To_Long_64_T (Right);
    return Result;
  end "-";
  function "-"
   (Left  : Long_T;
    Right : Long_64_T)
    return Long_64_T is
    Result : Long_64_T;
  begin
    Result := To_Long_64_T (Left) - Right;
    return Result;
  end "-";
  function "+"
   (Left, Right : in Real_T)
    return Real_T is
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Add);
    Result := (Value => Left.Value + Right.Value);
    return Result;
  end "+";
  function "-"
   (Left, Right : in Real_T)
    return Real_T is
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Sub);
    Result := (Value => Left.Value - Right.Value);
    return Result;
  end "-";

  function "-"
   (From : Long_64_T)
    return Long_64_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Sub_1);
    return (Long_64_T_Zero - From);
  end "-";
  function "abs"
   (From : Long_64_T)
    return Long_64_T is
    Value : Long_64_T := From;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Abs_1);
    if Value < Long_64_T (0)
    then
      Value := -Value;
    end if;
    return (Value);
  end "abs";
  function "+"
   (From : in Real_T)
    return Real_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Add_1);
    return From;
  end "+";
  function "-"
   (From : in Real_T)
    return Real_T is
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Sub_1);
    Result := (Value => -From.Value);
    return Result;
  end "-";
  function "abs"
   (From : in Real_T)
    return Real_T is
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Abs_1);
    Result := (Value => abs (From.Value));
    return Result;
  end "abs";
  function Increment
   (Le_Compteur : in Compteur_8_T)
    return Compteur_8_T is
  begin
    return Compteur_8_T'Succ (Le_Compteur);
  end Increment;
  function Increment
   (Le_Compteur : in Compteur_16_T)
    return Compteur_16_T is
  begin
    return Compteur_16_T'Succ (Le_Compteur);
  end Increment;

  function "*"
   (Left  : Long_64_T;
    Right : Long_T)
    return Long_64_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Mul);
    return Left * To_Long_64_T (Right);
  end "*";
  function Mult_32_To_64
   (Left  : Long_T;
    Right : Long_T)
    return Long_64_T is
    Result : Long_Long_Integer;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Mul);
    Result := Long_Long_Integer (Left) * Long_Long_Integer (Right);
    return Long_64_T (Result);
  end Mult_32_To_64;  function Mult_32_To_64
   (Left  : Unchecked_Long_T;
    Right : Unchecked_Long_T)
    return Long_64_T is
    Result : Long_Long_Integer;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Mul);
    Result := Long_Long_Integer (Left) * Long_Long_Integer (Right);
    return Long_64_T (Result);
  end Mult_32_To_64;  function "*"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Long_64_T is
    Result : Long_Long_Integer;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Mul);
    Result := Long_Long_Integer (Left) * Long_Long_Integer (Right);
    return Long_64_T (Result);
  end "*";
  function "/"
   (Left  : Long_64_T;
    Right : Long_T)
    return Long_64_T is
    Result : Long_Long_Integer;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Div);
    Result := Long_Long_Integer (Left) / Long_Long_Integer (Right);
    return Long_64_T (Result);
  end "/";
  function "/"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Long_64_T is
    Result : Long_Long_Integer;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Div);
    Result := Long_Long_Integer (Left) / Long_Long_Integer (Right);
    return Long_64_T (Result);
  end "/";
  function "mod"
   (Left  : in Long_64_T;
    Right : in Long_T)
    return Long_T is
    Result : Long_64_T;
  begin
    Result := Left mod To_Long_64 (Right);
    return To_Long (Result);
  end "mod";  function "mod"
   (Left  : in Long_64_T;
    Right : in Long_64_T)
    return Long_64_T is
    Division : Long_64_T;
    Result   : Long_64_T;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Mod);
    if (Left = Long_64_T_First and Right = Long_64_T_Minus_One)
    then
      return Long_64_T_Zero;
    else
    
      Division := Left / Right;

      Result := Left - (Right * Division);

    
      if ((Left < Long_64_T (0) and Right > Long_64_T (0)) or (Left > Long_64_T (0) and Right < Long_64_T (0)))
      and (Result /= Long_64_T (0))                                     
      then
        Result := Result + Right;
      end if;

      return Result;
    end if;
  end "mod";
  function Sqrt
   (From : Long_64_T)
    return Long_64_T is
    Root : Interfaces.Unsigned_64 := 0;
    M    : Interfaces.Unsigned_64 := 16#4000_0000_0000_0000#;
    X1   : Interfaces.Unsigned_64 := Interfaces.Unsigned_64 (From);
    X2   : Interfaces.Unsigned_64;
    use type Interfaces.Unsigned_64;
  begin
    loop
      X2   := Root + M;
      Root := Interfaces.Shift_Right (Root, 1);
      if X2 <= X1
      then
        X1   := X1 - X2;
        Root := Root + M;
      end if;
      M := Interfaces.Shift_Right (M, 2);
      exit when M = 0;
    end loop;
    return Long_64_T (Root);
  end Sqrt;

  function "*"
   (Left, Right : in Real_T)
    return Real_T is
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Mul);
    Result := (Value => Left.Value * Right.Value);
    return Result;
  end "*";
  function "/"
   (Left, Right : in Real_T)
    return Real_T is
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Div);
    Result := (Value => Left.Value / Right.Value);
    return Result;
  end "/";
  function "**"
   (Left  : in Real_T;
    Right :    Long_T)
    return Real_T is
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Result := Left * Left;
    if not (Right = Long_64_T (2))
    then
      raise Program_Error;
    end if;
    return Result;
  end "**";
  function Sqrt
   (From : in Real_T)
    return Real_T is
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Sqr);
    Result := (Value => Math.Sqrt (From.Value));
    return Result;
  end Sqrt;
  Shift_C : constant array (Shift_T) of Unchecked_Long_T :=
   (1  => 2**1,
    2  => 2**2,
    3  => 2**3,
    4  => 2**4,
    5  => 2**5,
    6  => 2**6,
    7  => 2**7,
    8  => 2**8,
    9  => 2**9,
    10 => 2**10,
    11 => 2**11,
    12 => 2**12,
    13 => 2**13,
    14 => 2**14,
    15 => 2**15,
    16 => 2**16,
    17 => 2**17,
    18 => 2**18,
    19 => 2**19,
    20 => 2**20,
    21 => 2**21,
    22 => 2**22,
    23 => 2**23,
    24 => 2**24,
    25 => 2**25,
    26 => 2**26,
    27 => 2**27,
    28 => 2**28,
    29 => 2**29,
    30 => 2**30);
  function Shift_Left
   (On     : in Unchecked_Byte_T;
    Shifts : in Shift_T)
    return Unchecked_Byte_T is
  
  
    Mask_Byte : constant Unchecked_Long_T := 16#0000_00FF#;
    Result    : constant Unchecked_Long_T := Shift_Left (Unchecked_Long_T (On), Shifts);
  begin
    return Unchecked_Byte_T (Result and Mask_Byte);
  end Shift_Left;  function Shift_Left
   (On     : in Unchecked_Word_T;
    Shifts : in Shift_T)
    return Unchecked_Word_T is
  
  
    Mask_Word : constant Unchecked_Long_T := 16#0000_FFFF#;
    Result    : constant Unchecked_Long_T := Shift_Left (Unchecked_Long_T (On), Shifts);
  begin
    return Unchecked_Word_T (Result and Mask_Word);
  end Shift_Left;
  function Shift_Left
   (On     : in Unchecked_Long_T;
    Shifts : in Shift_T)
    return Unchecked_Long_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Shift_Left);
    return Unchecked_Long_T'(On * Shift_C (Shifts));
  end Shift_Left;
  function Shift_Right
   (On     : in Unchecked_Byte_T;
    Shifts : in Shift_T)
    return Unchecked_Byte_T is
  begin
    return Unchecked_Byte_T (Shift_Right (Unchecked_Long_T (On), Shifts));
  end Shift_Right;
  function Shift_Right
   (On     : in Unchecked_Word_T;
    Shifts : in Shift_T)
    return Unchecked_Word_T is
  begin
    return Unchecked_Word_T (Shift_Right (Unchecked_Long_T (On), Shifts));
  end Shift_Right;
  function Shift_Right
   (On     : in Unchecked_Long_T;
    Shifts : in Shift_T)
    return Unchecked_Long_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Shift_Right);
    return On / Shift_C (Shifts);
  end Shift_Right;
  function Swap
   (From : in Byte_In_Bits_T)
    return Byte_In_Bits_T is
  begin
    case System.Default_Bit_Order is
      when System.Low_Order_First =>
        declare
          Result : Byte_In_Bits_T;
        begin
          for I in From'Range
          loop
            Result (I) := From (From'Length + 1 - I);
          end loop;
          return Result;
        end;
      when System.High_Order_First =>

      
      
        pragma Warnings (Off);
        return From;
        pragma Warnings (On);
    end case;
  end Swap;  function Byte
   (Byte_In_Bits : in Byte_In_Bits_T)
    return Unsigned_Byte_T is
  begin
    return Conv (Swap (Byte_In_Bits));
  end Byte;
  function Byte_In_Bits
   (Byte : in Unsigned_Byte_T)
    return Byte_In_Bits_T is
  begin
    return Swap (Conv (Byte));
  end Byte_In_Bits;
  function Lsb
   (W : in Unsigned_Word_T)
    return Unsigned_Byte_T is
  begin
    return Sw (W).Lsb;
  end Lsb;  function Msb
   (W : in Unsigned_Word_T)
    return Unsigned_Byte_T is
  begin
    return Sw (W).Msb;
  end Msb;  function Lsb
   (W : in Unchecked_Word_T)
    return Unchecked_Byte_T is
  begin
    return Sw (W).Lsb;
  end Lsb;  function Msb
   (W : in Unchecked_Word_T)
    return Unchecked_Byte_T is
  begin
    return Sw (W).Msb;
  end Msb;
  function Lsw
   (W : in Unsigned_Long_T)
    return Unsigned_Word_T is
  begin
    return Sl (W).Lsw;
  end Lsw;  function Msw
   (W : in Unsigned_Long_T)
    return Unsigned_Word_T is
  begin
    return Sl (W).Msw;
  end Msw;  function Lsw
   (W : in Unchecked_Long_T)
    return Unchecked_Word_T is
  begin
    return Sl (W).Lsw;
  end Lsw;  function Msw
   (W : in Unchecked_Long_T)
    return Unchecked_Word_T is
  begin
    return Sl (W).Msw;
  end Msw;
  function Lsw
   (From : in Long_64_T)
    return Unchecked_Long_T is
    Result : constant Unchecked_Long_T := To_Target_Long_64_T (From).Lsw;
  begin
    return (Result);
  end Lsw;  function Msw
   (From : in Long_64_T)
    return Long_T is
    Result : constant Long_T := To_Target_Long_64_T (From).Msw;
  begin
    return (Result);
  end Msw;  procedure Split
   (Item : in     Long_64_T;
    High :    out Long_T;
    Low  :    out Unchecked_Long_T) is
  begin
    High := To_Target_Long_64_T (Item).Msw;
    Low  := To_Target_Long_64_T (Item).Lsw;
  end Split;
  function To_Long
   (From : in Long_64_T)
    return Long_T is
    Lsw         : constant Unchecked_Long_T := To_Target_Long_64_T (From).Lsw;
    Msw         : constant Long_T           := To_Target_Long_64_T (From).Msw;
    Lsw_Rt      : Long_T;
    Tmp_As_Bits : Bit_Array_32_T;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Conv_To_Int);
    Tmp_As_Bits := To_Bit_Array_32 (Lsw);
    if Msw < Long_64_T (0)
    then
      Tmp_As_Bits (Tmp_As_Bits'Last) := 1;
    else
      Tmp_As_Bits (Tmp_As_Bits'Last) := 0;
    end if;
    Lsw_Rt := To_Long (Tmp_As_Bits);
    return (Lsw_Rt);
  end To_Long;  function Checked_To_Long
   (From : in Long_64_T)
    return Long_T is
    Lsw             : constant Unchecked_Long_T := To_Target_Long_64_T (From).Lsw;
    Msw             : constant Long_T           := To_Target_Long_64_T (From).Msw;
    Lsw_Rt          : Long_T;
    Tmp_Low_As_Bits : constant Bit_Array_32_T   := To_Bit_Array_32 (Lsw);
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Conv_To_Int);
    Lsw_Rt := To_Long (Tmp_Low_As_Bits);
    if ((Msw = Long_T (0)) and (Lsw_Rt >= Long_T (0))) or ((Msw = Long_T (-1)) and (Lsw_Rt < Long_T (0)))
    then
      return Lsw_Rt;
    else
      raise Constraint_Error;
    end if;
  end Checked_To_Long;
  function To_Unsigned_Long
   (From : in Long_64_T)
    return Unsigned_Long_T is
    Lsw   : Long_T := Long_T (To_Target_Long_64_T (From).Lsw);
    Value : Unsigned_Long_T;
    for Value'Address use Lsw'Address;
    pragma Import (Ada, Value);
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Conv_To_Int);
    return (Value);
  end To_Unsigned_Long;  function Managed_To_Unsigned_Long
   (From : in Long_64_T)
    return Unsigned_Long_T is
    Tmp_As_Bits : Bit_Array_32_T;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Conv_To_Int);
    Tmp_As_Bits                    := To_Bit_Array_32 (To_Target_Long_64_T (From).Lsw);
    Tmp_As_Bits (Tmp_As_Bits'Last) := 0;
    return (To_Unsigned_Long (Tmp_As_Bits));
  end Managed_To_Unsigned_Long;
  function Make_Word
   (Msb : in Unsigned_Byte_T;
    Lsb : in Unsigned_Byte_T)
    return Unsigned_Word_T is
    Splitted_Word : constant Unsigned_Sw_T := (Msb => Msb, Lsb => Lsb);
  begin
    return Uw (Splitted_Word);
  end Make_Word;  function Make_Word
   (Msb : in Unchecked_Byte_T;
    Lsb : in Unchecked_Byte_T)
    return Unchecked_Word_T is
    Splitted_Word : constant Unchecked_Sw_T := (Msb => Msb, Lsb => Lsb);
  begin
    return Uw (Splitted_Word);
  end Make_Word;
  function Make_Long
   (Msw : in Unchecked_Word_T;
    Lsw : in Unchecked_Word_T)
    return Unchecked_Long_T is
    Splitted_Long : constant Unchecked_Sl_T := (Msw => Msw, Lsw => Lsw);
  begin
    return Ul (Splitted_Long);
  end Make_Long;

  function To_Long_64
   (From : in Long_T)
    return Long_64_T is
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Conv_From_Int);
    return Long_64_T (From);
  end To_Long_64;  function To_Long_64
   (From : in Unsigned_Long_T)
    return Long_64_T is
  
    Lsw   : constant Unchecked_Long_T := Unchecked_Long_T (From);
    Value : Long_64_T;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Conv_From_Int);
    Value := Make_Long_64
      (Msw => 0,
       Lsw => Lsw);
    return (Value);
  end To_Long_64;  function Make_Long_64
   (Msw : in Long_T;
    Lsw : in Unchecked_Long_T)
    return Long_64_T is
    Target : Target_Long_64_T;
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Conv_From_Int);
    Target.Msw := Msw;
    Target.Lsw := Lsw;
    return To_Long_Long_Integer (Target);
  end Make_Long_64;  function Real_Of_Int
   (From : in T)
    return Real_T is
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Counter.Increment_Operation_Cnt (Counter.Float_Conv_From_Int);
    Result := (Value => Float (From));
    return Result;
  end Real_Of_Int;
  function Real_Of_Fixed
   (From : in T)
    return Real_T is
    function To_Real is new Real_Of_Int (Long_T);
    function To_Int is new Unchecked_Conversion (T, Long_T);
    Small  : constant Real_T := (Value => T'Small);
    Result : Real_T;
    pragma Volatile (Result);
  begin
    Result := Small * To_Real (To_Int (From));
    return Result;
  end Real_Of_Fixed;  function To_Real
   (From : in Long_64_T)
    return Real_T is
    Real_Value : Real_T;
    pragma Volatile (Real_Value);
  begin
    Counter.Increment_Operation_Cnt (Counter.Int64_Conv_To_Real);
    Real_Value := (Value => Float (From));
    return Real_Value;
  end To_Real;

  function To_Long_64
   (From : in Real_T)
    return Long_64_T is
  begin
    return Long_64_T (From.Value);
  end To_Long_64;
  function Int_Of_Real
   (From : in Real_T)
    return T is
    From_V : Real_T;
    pragma Volatile (From_V);
  begin
    From_V := From;
    Counter.Increment_Operation_Cnt (Counter.Float_Conv_To_Int);
    return T (From_V.Value);
  end Int_Of_Real;  function Fixed_Of_Real
   (From : in Real_T)
    return T is
    function From_Real is new Int_Of_Real (Long_T);
    function From_Int is new Unchecked_Conversion (Long_T, T);
    Small  : constant Real_T := (Value => T'Small);
    From_V : Real_T;
    pragma Volatile (From_V);
  begin
    From_V := From;
    return From_Int (From_Real (From_V / Small));
  end Fixed_Of_Real;
end Types_Alstom;
