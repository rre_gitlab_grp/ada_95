with Interlocking_Mgr;
with Interlocking_Mgr.Internal_Interlocking_Mgr;
with Ada.Text_IO;
with Point;
with Types_Alstom;

package body Interlocking_Mgr is
  function Get_Points_At_Pos
   (Pos  : in Points_Card_T)
    return Interlocking_Mgr_Points_Container.Element_Ptr is
  begin
    return Interlocking_Mgr_Points_K_List.Get_Element_At_Pos
     (Self => Interlocking_Mgr.Internal_Interlocking_Mgr.Interlocking_Mgr_Unique_Instance.Points_Lst'Access,
      Pos  => Pos);
  end Get_Points_At_Pos;

  procedure Init_Points is
    Index                       : Integer := 0;
    Id                          : Rbc_Data_Types.Identifier_T ;
    Position                    : Points_Card_T;
    Array_List                  : Array_T := (1 => "Points_000000001",
                                              2 => "Points_000000002",
                                              3 => "Points_000000003",
                                              4 => "Points_000000004");

  begin

    for I in Points_Card_T'Range
    loop
      Index := Index + 1;

      for J in Id'Range loop
        Id (J) := Array_List (Index) (J);
      end loop;

      Interlocking_Mgr_Points_K_List.Add_New (Self => Interlocking_Mgr.Internal_Interlocking_Mgr.Interlocking_Mgr_Unique_Instance.Points_Lst,
                                              Id   => Id ,
                                              Pos  => Position);

      Ada.Text_IO.Put_Line ("Position is" & Points_Card_T'Image (Position));

      Ada.Text_IO.Put_Line ("String is:" & Id);
    end loop;

  end Init_Points;

  procedure Init_Point_Novram is
    Point_Index         : Types_Alstom.Natural_Long_T := Types_Alstom.Natural_Long_T'Last;
    This_Point          : Interlocking_Mgr_Points_Container.Element_Ptr;
  begin

    for Point_Index in 1 .. Dyn_Constants.Nmax_Point loop
      This_Point := Interlocking_Mgr.Get_Points_At_Pos (Pos => Points_Card_T (Point_Index));

      Point.Set_Point_State
       (This  => This_Point.all,
        Value => Rm_Data_Types.Blocked);
      Ada.Text_IO.Put_Line ("Init_Point_Novram() for Position is:" & Points_Card_T'Image (Points_Card_T (Point_Index)));
    end loop;
  end Init_Point_Novram;
  procedure Code_Point_Novram is

    function Is_Point_Currently_Controlled
     (This : access Point.Point_T)
      return Types_Alstom.Boolean_T is
      use type Rm_Data_Types.Point_State_T;
    begin

      return (Point.Get_Point_State (This => This.all) = Rm_Data_Types.Blocked);
    end Is_Point_Currently_Controlled;

    Point_Iterator : Interlocking_Mgr_Points_Container.Iterator_T;
    This_Point     : Point.Point_Acc_T;
    N_Point        : Integer := 0;
  begin

    Point_Iterator :=
     Interlocking_Mgr_Points_K_List.New_Iterator (Self => Interlocking_Mgr.Internal_Interlocking_Mgr.Interlocking_Mgr_Unique_Instance.Points_Lst);
    Find_N_Points_Loop :

    while not Interlocking_Mgr_Points_K_List.Done
     (It   => Point_Iterator,
      Self => Interlocking_Mgr.Internal_Interlocking_Mgr.Interlocking_Mgr_Unique_Instance.Points_Lst)
    loop

      This_Point :=
       Interlocking_Mgr_Points_K_List.Get
        (It   => Point_Iterator,
         Self => Interlocking_Mgr.Internal_Interlocking_Mgr.Interlocking_Mgr_Unique_Instance.Points_Lst'Access);

      Ada.Text_IO.Put_Line ("\n Code_Point_Novram :Point Key:" & Point.Get_Id (This => This_Point.all));

      if Is_Point_Currently_Controlled (This => This_Point) then
        N_Point := N_Point + 1;
      end if;

      Interlocking_Mgr_Points_K_List.Next
       (It   => Point_Iterator,
        Self => Interlocking_Mgr.Internal_Interlocking_Mgr.Interlocking_Mgr_Unique_Instance.Points_Lst);
    end loop Find_N_Points_Loop;

    Ada.Text_IO.Put_Line ("\n Code_Point_Novram() N_Point:" & Integer'Image (N_Point));

  end Code_Point_Novram;

  procedure Code_Point_Novram_New is
    function Is_Point_Currently_Controlled
     (This : access Point.Point_T)
      return Types_Alstom.Boolean_T is
      use type Rm_Data_Types.Point_State_T;
    begin

      return (Point.Get_Point_State (This => This.all) = Rm_Data_Types.Blocked);
    end Is_Point_Currently_Controlled;

    This_Point          : Interlocking_Mgr_Points_Container.Element_Ptr;
    Point_Index         : Types_Alstom.Natural_Long_T := Types_Alstom.Natural_Long_T'Last;
    N_Point             : Integer := 0;

  begin

    for Point_Index in 1 .. Dyn_Constants.Nmax_Point loop
      This_Point := Interlocking_Mgr.Get_Points_At_Pos (Pos => Points_Card_T (Point_Index));
      if Is_Point_Currently_Controlled (This => This_Point) then
        N_Point := N_Point + 1;
      end if;
    end loop;
    Ada.Text_IO.Put_Line ("\n Code_Point_Novram_New() N_Point:" & Integer'Image (N_Point));
  end Code_Point_Novram_New;
  function Get_Point_Pos_From_Key
   (Id   : in Rbc_Data_Types.Identifier_T)
    return Rm_Data_Types.Index_Point_T is
  begin
    return Interlocking_Mgr_Points_K_List.Get_Pos_From_Key
     (Self => Internal_Interlocking_Mgr.Interlocking_Mgr_Unique_Instance.Points_Lst,
      Id   => Id);
  end Get_Point_Pos_From_Key;

  procedure Remove_Point_At_Pos (Pos : in Points_Card_T) is
  begin
    Interlocking_Mgr_Points_K_List.Remove_Element_At_Pos
     (Self => Interlocking_Mgr.Internal_Interlocking_Mgr.Interlocking_Mgr_Unique_Instance.Points_Lst,
      Pos  => Pos);
  end Remove_Point_At_Pos;

end Interlocking_Mgr;
