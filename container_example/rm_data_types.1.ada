
with Dyn_Constants;
pragma Elaborate_All (Dyn_Constants);
package Rm_Data_Types is
  type Point_Position_T is (Right, Left, In_Movement, Undefined);
  pragma Ordered (Point_Position_T);
  type Point_State_T is (No_Local_Control, Local_Control, Blocked);
  type Index_Node_Base is range 0 .. 2 ** (16) - 1;
  subtype Index_Node_T is Index_Node_Base range 0 .. Index_Node_Base (100);
  type Range_Connectors_Per_Node_T is range 0 .. 2;

 type Index_Point_Base is range 0 .. 2 ** 16 - 1;
  subtype Index_Point_T is Index_Point_Base range 0 .. Index_Point_Base (Dyn_Constants.Nmax_Point);

  Point_State_D             : constant Point_State_T             := No_Local_Control;
  Point_Position_D          : constant Point_Position_T          := Undefined;

end Rm_Data_Types;
