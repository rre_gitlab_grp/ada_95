
package body Point is
  function Get_Id
   (This : in Point_T)
    return Rbc_Data_Types.Identifier_T is
  begin
    return This.Id;
  end Get_Id;

  procedure Set_Id
   (This  : in out Point_T;
    Value : in Rbc_Data_Types.Identifier_T) is
  begin
    This.Id := Value;
  end Set_Id;

  function Get_Point_State
   (This : in Point_T)
    return Rm_Data_Types.Point_State_T is
  begin
    return This.Point_State;
  end Get_Point_State;

  procedure Set_Point_State
   (This  : in out Point_T;
    Value : in Rm_Data_Types.Point_State_T) is
  begin
    This.Point_State := Value;
  end Set_Point_State;

  function Get_Point_Position
   (This : in Point_T)
    return Rm_Data_Types.Point_Position_T is
  begin
    return This.Point_Position;
  end Get_Point_Position;

  procedure Set_Point_Position
   (This  : in out Point_T;
    Value : in Rm_Data_Types.Point_Position_T) is
  begin
    This.Point_Position := Value;
  end Set_Point_Position;

  function Get_Ixl_Point_State
   (This : in Point_T)
    return Rm_Data_Types.Point_State_T is
  begin
    return This.Ixl_Point_State;
  end Get_Ixl_Point_State;

  function Get_Ixl_Point_Position
   (This : in Point_T)
    return Rm_Data_Types.Point_Position_T is
  begin
    return This.Ixl_Point_Position;
  end Get_Ixl_Point_Position;

  procedure Init_Object (This : access Point_T) is
  begin
    This.Point_State                := Rm_Data_Types.Point_State_D;
    This.Point_Position             := Rm_Data_Types.Point_Position_D;
    This.Ixl_Point_State            := Rm_Data_Types.Point_State_D;
    This.Ixl_Point_Position         := Rm_Data_Types.Point_Position_D;
    This.Node_Index                 := Rbc_Constants.Null_Pos;
    This.Node_Left_Connector_Index  := 0;
    This.Node_Right_Connector_Index := 0;
  end Init_Object;
  procedure Init
   (This                       : in out Point_T;
    Ixl_Point_State            : in Rm_Data_Types.Point_State_T;
    Ixl_Point_Position         : in Rm_Data_Types.Point_Position_T;
    Node_Index                 : Rm_Data_Types.Index_Node_T;
    Node_Left_Connector_Index  : Rm_Data_Types.Range_Connectors_Per_Node_T;
    Node_Right_Connector_Index : Rm_Data_Types.Range_Connectors_Per_Node_T) is
  begin
    This.Point_State                := Rm_Data_Types.Point_State_D;
    This.Point_Position             := Rm_Data_Types.Point_Position_D;
    This.Ixl_Point_State            := Ixl_Point_State;
    This.Ixl_Point_Position         := Ixl_Point_Position;
    This.Node_Index                 := Node_Index;
    This.Node_Left_Connector_Index  := Node_Left_Connector_Index;
    This.Node_Right_Connector_Index := Node_Right_Connector_Index;
  end Init;
  procedure Manage_Force_Unblock (This : access Point_T) is
  begin
      This.Point_State := Rm_Data_Types.Local_Control;
  end Manage_Force_Unblock;
  procedure Update
   (This               : in out Point_T;
    Ixl_Point_State    : in Rm_Data_Types.Point_State_T;
    Ixl_Point_Position : in Rm_Data_Types.Point_Position_T) is

  begin
    This.Ixl_Point_State    := Ixl_Point_State;
    This.Ixl_Point_Position := Ixl_Point_Position;
  end Update;

  procedure Update_Point_States_And_Positions (This : in out Point_T) is

    use type Rm_Data_Types.Point_Position_T;

  begin

    case This.Ixl_Point_State is
      when Rm_Data_Types.No_Local_Control =>
        case This.Point_State is
          when Rm_Data_Types.No_Local_Control =>
            null;
          when Rm_Data_Types.Blocked =>
          
          
          
            This.Point_Position := This.Ixl_Point_Position;

          when Rm_Data_Types.Local_Control =>
            This.Point_State    := Rm_Data_Types.No_Local_Control;
            This.Point_Position := This.Ixl_Point_Position;

        end case;

      when Rm_Data_Types.Local_Control =>
        case This.Point_State is
          when Rm_Data_Types.Blocked =>
          
          
          
            This.Point_Position := This.Ixl_Point_Position;

          when Rm_Data_Types.No_Local_Control =>
            This.Point_State    := Rm_Data_Types.Local_Control;
            This.Point_Position := This.Ixl_Point_Position;

          when Rm_Data_Types.Local_Control =>
            case This.Ixl_Point_Position is
              when Rm_Data_Types.Undefined =>
                This.Point_Position := Rm_Data_Types.Undefined;

              when Rm_Data_Types.In_Movement =>
                null;

              when Rm_Data_Types.Left =>
                if This.Point_Position /= Rm_Data_Types.Right then
                  This.Point_Position := This.Ixl_Point_Position;
                end if;

              when Rm_Data_Types.Right =>
                if This.Point_Position /= Rm_Data_Types.Left then
                  This.Point_Position := This.Ixl_Point_Position;
                end if;

            end case;
        end case;

      when Rm_Data_Types.Blocked =>
        case This.Point_State is
          when Rm_Data_Types.No_Local_Control =>
            This.Point_State := Rm_Data_Types.Blocked;

          when Rm_Data_Types.Local_Control =>
            This.Point_State := Rm_Data_Types.Blocked;

          when Rm_Data_Types.Blocked =>
            null;

        end case;
    end case;

  end Update_Point_States_And_Positions;

  procedure Get_Node_Position
   (This          : in Point_T;
    Node_Index    : out Rm_Data_Types.Index_Node_T;
    Node_Position : out Rm_Data_Types.Range_Connectors_Per_Node_T) is

    use type Rm_Data_Types.Index_Node_T;

  begin

    Node_Index    := This.Node_Index;
    Node_Position := 0;

    if Node_Index /= Rbc_Constants.Null_Pos then

      case This.Ixl_Point_Position is
        when Rm_Data_Types.Left =>
          Node_Position := This.Node_Left_Connector_Index;
        when Rm_Data_Types.Right =>
          Node_Position := This.Node_Right_Connector_Index;
        when others =>
          null;
      end case;

    end if;

  end Get_Node_Position;

end Point;
