
with Ada.Text_IO;
package body Container_G.List_G is
  function Get_Element_At_Pos
   (Self : access List_T;
    Pos  : in     Index_Range_T)
    return Element_Ptr is
  begin
    if not Self.T_Status (Pos) then
      Ada.Text_IO.Put_Line("\n Container_G.List_G::Get_Element_At_Pos() Element Not valid");
    end if;
    return Self.T_Element (Pos)'Access;

  end Get_Element_At_Pos;
  function Get_Element_At_Pos
   (Self : in List_T;
    Pos  : in Index_Range_T)
    return Element_T is
  begin
    if not Self.T_Status (Pos) then
      Ada.Text_IO.Put_Line("\n Container_G.List_G::Get_Element_At_Pos() Element Not valid 2");
    end if;
    return Self.T_Element (Pos);
  end Get_Element_At_Pos;
  procedure Add_New
   (Self : in out List_T;
    Pos  :    out Pos_Range_T) is

    Free_Found : Boolean := False;
  begin
  
    if Self.First_Free = Rbc_Constants.Null_Pos then
      Pos := Rbc_Constants.Null_Pos;
    else
      Self.Size                       := Self.Size + 1;
      Self.T_Status (Self.First_Free) := True;
      Pos                             := Self.First_Free;
    
    
      if Self.First_Relevant not in Rbc_Constants.Null_Pos + 1 .. Self.First_Free then
        Self.First_Relevant := Self.First_Free;
      end if;
    
    
      while not (Free_Found or Self.First_Free = Rbc_Constants.Null_Pos) loop
        if Self.First_Free = Pos_Range_T'Last then
          Self.First_Free := Rbc_Constants.Null_Pos;
        else
          Self.First_Free := Self.First_Free + 1;
          if not Self.T_Status (Self.First_Free) then
            Free_Found := True;
          end if;
        end if;
      end loop;
    
    end if;
  end Add_New;

  procedure Add_New_At_Pos
   (Self : in out List_T;
    Pos  : in out Index_Range_T) is

    Free_Found : Boolean := False;
  begin
  
    if Self.T_Status (Pos) then
      Ada.Text_IO.Put_Line("\n Container_G.List_G::Add_New_At_Pos() Element Not valid");
    else
    
      Self.Size           := Self.Size + 1;
      Self.T_Status (Pos) := True;
      Init (Self.T_Element (Pos)'Access);
    
    
      if Self.First_Relevant = Rbc_Constants.Null_Pos or Pos < Self.First_Relevant then
        Self.First_Relevant := Pos;
      end if;
    
    
      if Self.First_Free = Pos
      then
      
        while not (Free_Found or Self.First_Free = Rbc_Constants.Null_Pos) loop
          if Self.First_Free = Pos_Range_T'Last then
            Self.First_Free := Rbc_Constants.Null_Pos;
          else
            Self.First_Free := Self.First_Free + 1;
            if not Self.T_Status (Self.First_Free) then
              Free_Found := True;
            end if;
          end if;
        end loop;
      end if;
    
    end if;
  end Add_New_At_Pos;
  procedure Clear (Self : out List_T) is
  begin
    Self.T_Status       := (others => False);
    Self.First_Free     := Init_First_Free;
    Self.First_Relevant := Rbc_Constants.Null_Pos;
    Self.Size           := Empty;
  end Clear;

  procedure Remove_Element_At_Pos
   (Self : in out List_T;
    Pos  : in     Index_Range_T) is

    Relevant_Found : Boolean := False;
  begin
  
    if not Self.T_Status (Pos) then
      Ada.Text_IO.Put_Line("\n Container_G.List_G::Remove_Element_At_Pos() Element Not valid ");
    end if;

    Self.Size := Self.Size - 1;

    Self.T_Status (Pos) := False;
  
  
    if Self.First_Free not in Rbc_Constants.Null_Pos + 1 .. Pos then
      Self.First_Free := Pos;
    end if;
  

  
    if Self.First_Relevant = Pos then
      while not (Relevant_Found or Self.First_Relevant = Rbc_Constants.Null_Pos) loop
        if Self.First_Relevant = Pos_Range_T'Last then
          Self.First_Relevant := Rbc_Constants.Null_Pos;
        else
          Self.First_Relevant := Self.First_Relevant + 1;
          if Self.T_Status (Self.First_Relevant) then
            Relevant_Found := True;
          end if;
        end if;
      end loop;
    end if;
  
  end Remove_Element_At_Pos;
  procedure Next
   (It   : in out Iterator_T;
    Self : in     List_T) is

    Relevant_Found : Boolean := False;
  begin
    if It = Rbc_Constants.Null_Pos then
      Ada.Text_IO.Put_Line("\n Container_G.List_G::Next() Element Not valid ");
    end if;
    while not (Relevant_Found or It = Rbc_Constants.Null_Pos) loop
      if It = Pos_Range_T'Last then
        It := Rbc_Constants.Null_Pos;
      else
        It := It + 1;
        if Self.T_Status (It) then
          Relevant_Found := True;
        end if;
      end if;
    end loop;
  end Next;
  function New_Iterator
   (Self : in List_T)
    return Iterator_T is
  begin
    return Self.First_Relevant;
  end New_Iterator;
  procedure Execute (Self      : access List_T;
                     This_Proc : in Proc_Access_T) is
    Iterator : Iterator_T := New_Iterator (Self.all);
  begin
    while not Done (Iterator, Self.all) loop
      This_Proc (Self.T_Element (Iterator)'Access);
      Next (Iterator, Self.all);
    end loop;
  end Execute;
  procedure Execute (Self      : access List_T;
                     This_Proc : in Proc_Idx_Access_T) is
    Iterator : Iterator_T := New_Iterator (Self.all);
  begin
    while not Done (Iterator, Self.all) loop
      This_Proc (Self.T_Element (Iterator)'Access, Iterator);
      Next (Iterator, Self.all);
    end loop;
  end Execute;
  function Get
   (It   : in     Iterator_T;
    Self : access List_T)
    return Element_Ptr is
  begin
    if It = Rbc_Constants.Null_Pos or else not Self.T_Status (It) then
      Ada.Text_IO.Put_Line("\n Container_G.List_G::Get() Iterator_Not_Valid ");
    end if;
    return Self.T_Element (It)'Access;
  end Get;
  function Get
   (It   : in Iterator_T;
    Self : in List_T)
    return Element_T is
  begin
    if It = Rbc_Constants.Null_Pos or else not Self.T_Status (It) then
      Ada.Text_IO.Put_Line("\n Container_G.List_G::Get2() Iterator_Not_Valid");
    end if;
    return Self.T_Element (It);
  end Get;
  function Getstatus
   (Self : in List_T;
    Pos  : in Index_Range_T)
    return Boolean is
  begin
    return Self.T_Status (Pos);
  end Getstatus;
  function Init_First_Free return Pos_Range_T is
    First_Free_Value : Pos_Range_T;
  begin
    if Full = Rbc_Constants.Null_Pos then
    
      First_Free_Value := Rbc_Constants.Null_Pos;
    else
    
      First_Free_Value := Index_Range_T'First;
    end if;
    return First_Free_Value;
  end Init_First_Free;

end Container_G.List_G;
