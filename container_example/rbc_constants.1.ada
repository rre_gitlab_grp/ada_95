package Rbc_Constants is
  Null_Pos      : constant := 0;
  Irrelevant_Id : constant String := "                ";
  Nmax_Mc_Bits  : constant := 32;
end Rbc_Constants;
