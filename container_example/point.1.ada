


with Rbc_Data_Types;
with Rm_Data_Types;
with Rbc_Constants;
package Point is
  type Point_T is private;
  type Point_Acc_T is access all Point_T;
  function Get_Id
   (This : in Point_T)
    return Rbc_Data_Types.Identifier_T;
  pragma Inline (Get_Id);

  procedure Set_Id
   (This  : in out Point_T;
    Value : in Rbc_Data_Types.Identifier_T);
  pragma Inline (Set_Id);

  function Get_Point_State
   (This : in Point_T)
    return Rm_Data_Types.Point_State_T;
  pragma Inline (Get_Point_State);

  procedure Set_Point_State
   (This  : in out Point_T;
    Value : in Rm_Data_Types.Point_State_T);
  pragma Inline (Set_Point_State);

  function Get_Point_Position
   (This : in Point_T)
    return Rm_Data_Types.Point_Position_T;
  pragma Inline (Get_Point_Position);

  procedure Set_Point_Position
   (This  : in out Point_T;
    Value : in Rm_Data_Types.Point_Position_T);
  pragma Inline (Set_Point_Position);

  function Get_Ixl_Point_State
   (This : in Point_T)
    return Rm_Data_Types.Point_State_T;
  pragma Inline (Get_Ixl_Point_State);

  function Get_Ixl_Point_Position
   (This : in Point_T)
    return Rm_Data_Types.Point_Position_T;
  pragma Inline (Get_Ixl_Point_Position);
  procedure Init_Object (This : access Point_T);
  pragma Inline (Init_Object);

  procedure Init
   (This                       : in out Point_T;
    Ixl_Point_State            : in Rm_Data_Types.Point_State_T;
    Ixl_Point_Position         : in Rm_Data_Types.Point_Position_T;
    Node_Index                 : Rm_Data_Types.Index_Node_T;
    Node_Left_Connector_Index  : Rm_Data_Types.Range_Connectors_Per_Node_T;
    Node_Right_Connector_Index : Rm_Data_Types.Range_Connectors_Per_Node_T);
  procedure Manage_Force_Unblock (This : access Point_T);

  procedure Update
   (This               : in out Point_T;
    Ixl_Point_State    : in Rm_Data_Types.Point_State_T;
    Ixl_Point_Position : in Rm_Data_Types.Point_Position_T);
  pragma Inline (Update);

  procedure Update_Point_States_And_Positions (This : in out Point_T);

  procedure Get_Node_Position
   (This          : in Point_T;
    Node_Index    : out Rm_Data_Types.Index_Node_T;
    Node_Position : out Rm_Data_Types.Range_Connectors_Per_Node_T);
  pragma Inline (Get_Node_Position);

private
  type Point_T is
     record
       Id                         : Rbc_Data_Types.Identifier_T    := Rbc_Constants.Irrelevant_Id;
       Point_State                : Rm_Data_Types.Point_State_T    := Rm_Data_Types.Point_State_D;
       Point_Position             : Rm_Data_Types.Point_Position_T := Rm_Data_Types.Point_Position_D;
       Ixl_Point_State            : Rm_Data_Types.Point_State_T    := Rm_Data_Types.Point_State_D;
       Ixl_Point_Position         : Rm_Data_Types.Point_Position_T := Rm_Data_Types.Point_Position_D;
       Node_Index                 : Rm_Data_Types.Index_Node_T;
       Node_Left_Connector_Index  : Rm_Data_Types.Range_Connectors_Per_Node_T;
       Node_Right_Connector_Index : Rm_Data_Types.Range_Connectors_Per_Node_T;
     end record;

end Point;
