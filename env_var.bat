@echo off

setlocal

Set Cur_Dir=%cd%

set EDUCATION=NO
Set Run_Param=NO

if "%CI_JOB_NAME%" == "run_all_scenarios_with_education" (
  set EDUCATION=YES
)
if "%CI_JOB_NAME%" == "daily_work_nightly" (
  set EDUCATION=YES
)

if "%EDUCATION%" == "YES" (
  sleep 60
)
