with Ada.Numerics;
package body Satellite is

   Earth_Radius : constant := 3963.0;

   function Input_Values_In_Range
     (Data : Data_T)
     return Boolean is
   begin
      if Data = null
      then
         return False;
      end if;
      return Data.Altitude in Altitude_Range_T
        and then Data.Number_Of_Orbits in Number_Of_Orbits_Range_T
        and then Data.Time_Orbiting in Time_Orbiting_Range_T;
   end Input_Values_In_Range;

   function Velocity
     (Data : not null Data_T)
     return Real_T is
      Orbit_Radius  : Real_T;
      Circumference : Real_T;
      Distance      : Real_T;
   begin
      Orbit_Radius  := Earth_Radius + Data.Altitude;
      Circumference := 2.0 * Ada.Numerics.Pi * Orbit_Radius;
      Distance      := Data.Number_Of_Orbits * Circumference;
      return Distance / Data.Time_Orbiting;
   end Velocity;

   function Velocity
     (Data : not null Data_T)
     return String is
      Ret_Val : Real_T;
   begin
      Ret_Val := Velocity (Data);
      return Ret_Val'Image & "mph";
   end Velocity;

end Satellite;
