with My_Text_Io;
with Satellite;
with Satellite_Data; use Satellite_Data;
with Satellite.Pretty_Print;
with Pretty_Print;
procedure Display_Velocities is

   procedure Update_Input_Value
     (Prompt :        String;
      Value  : in out Satellite.Real_T) is
      Input : String (1 .. 20);
      Last  : Integer range Input'First - 1 .. Input'Last;
   begin
      loop
         My_Text_Io.Put
           ("Enter " & Prompt & " (return to use" & Value'Image & "): ");
         My_Text_Io.Get_Line (Input, Last);
         exit when Last = Input'First - 1;
         Value := Satellite.Real_T'Value (Input
                (Input'First .. Last));
      end loop;
   end Update_Input_Value;

begin
   for Agency in Space_Agencies_T
   loop
      -- get values
      Update_Input_Value
        (Agency'Image & " altitude", Database (Agency).Altitude);
      Update_Input_Value
        (Agency'Image & " number_of orbits",
         Database (Agency).Number_Of_Orbits);
      Update_Input_Value
        (Agency'Image & " time orbiting", Database (Agency).Time_Orbiting);
   end loop;
   if not
     (for all Agency in Space_Agencies_T =>
        Satellite.Input_Values_In_Range (Database (Agency)'Access))
   then
      My_Text_Io.Put_Line ("Not all satellite data is valid");
   end if;
   for Agency in Space_Agencies_T
   loop
      -- display values
      My_Text_Io.Put (Agency'Image & " => ");
      Database (Agency).Velocity :=
        Satellite.Velocity (Database (Agency)'Access);
      My_Text_Io.Put_Line
        ("Velocity = " & Satellite.Velocity (Database (Agency)'Access));
   end loop;
   if
     (for some Agency in Space_Agencies_T =>
        Database (Agency).Velocity not in Satellite.Velocity_Range_T)
   then
      My_Text_Io.Put_Line ("A velocity is out of range");
   end if;

   for Agency in Space_Agencies_T
   loop
      Satellite.Pretty_Print.Print
        ("Debug " & Agency'Image, Database (Agency)'Access);
      Pretty_Print
        ("Feet Per Second " & Agency'Image, Database (Agency)'Access);
   end loop;
end Display_Velocities;
