with Ada.Text_Io;
package body My_Text_Io is
   procedure Put (Str : String) is
   begin
      Ada.Text_Io.Put (Str);
   end Put;
   procedure Put_Line (Str : String) is
   begin
      Ada.Text_Io.Put_Line (Str);
   end Put_Line;
   procedure Get_Line
     (Str    : in out String;
      Length :    out Natural) is
   begin
      Ada.Text_Io.Get_Line (Str, Length);
   end Get_Line;
end My_Text_Io;
