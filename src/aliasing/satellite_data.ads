with Satellite;
package Satellite_Data is

   type Space_Agencies_T is (Nasa, Esa, Rsa);

   -- initialize with "valid" values
   Database : array (Space_Agencies_T) of aliased Satellite.Data_Record_T :=
     (Nasa => (1.2, 15.0, 100.0, 0.0),
      Esa  =>
        (Number_Of_Orbits => 3.4, Time_Orbiting => 15.0, Altitude => 200.0,
         Velocity         => 0.0),
      Rsa =>
        (Altitude => 300.0, Time_Orbiting => 15.0, Number_Of_Orbits => 5.6,
         Velocity => 0.0));

end Satellite_Data;
