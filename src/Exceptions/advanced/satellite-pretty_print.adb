with My_Text_Io;
package body Satellite.Pretty_Print is
   procedure Print
     (Prompt : String;
      Data   : Data_T) is
   begin
      My_Text_Io.Put_Line (Prompt);
      My_Text_Io.Put_Line
        ("   Number_Of_Orbits => " & Data.Number_Of_Orbits'Image);
      My_Text_Io.Put_Line
        ("   Time_Orbiting    => " & Data.Time_Orbiting'Image);
      My_Text_Io.Put_Line ("   Altitude         => " & Data.Altitude'Image);
      My_Text_Io.Put_Line ("   Velocity         => " & Data.Velocity'Image);
   end Print;
end Satellite.Pretty_Print;
