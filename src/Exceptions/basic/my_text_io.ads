package My_Text_Io is
   procedure Put (Str : String);
   procedure Put_Line (Str : String);
   procedure Get_Line
     (Str    : in out String;
      Length :    out Natural);
end My_Text_Io;
