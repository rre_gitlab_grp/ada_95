package Satellite is
   type Real_T is digits 6;

   subtype Altitude_Range_T is Real_T range 50.0 .. 1000.0;
   subtype Number_Of_Orbits_Range_T is Real_T range 0.1 .. Real_T'Last;
   subtype Time_Orbiting_Range_T is Real_T range 0.1 .. Real_T'Last;
   subtype Velocity_Range_T is Real_T range 1_000.0 .. 50_000.0;

   type Data_T is record
      Number_Of_Orbits : Real_T;
      Time_Orbiting    : Real_T;
      Altitude         : Real_T;
      Velocity         : Real_T;
   end record;

   function Input_Values_In_Range
     (Data : Data_T)
     return Boolean;

   function Velocity
     (Data : Data_T)
     return Real_T;

   function Velocity
     (Data : Data_T)
     return String;

end Satellite;
