
package body Score is

   -------------------------
   -- Dispaly_All_Private --
   -------------------------

   procedure Dispaly_All_Private is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Dispaly_All_Private unimplemented");
      raise Program_Error with "Unimplemented procedure Dispaly_All_Private";
   end Dispaly_All_Private;

end Score;
