pragma Ada_2012;
package body Score.Display is

   -----------------
   -- Show_Scores --
   -----------------

   procedure Show_Scores is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Show_Scores unimplemented");
      raise Program_Error with "Unimplemented procedure Show_Scores";
   end Show_Scores;

   ----------------
   -- Show_Fouls --
   ----------------

   procedure Show_Fouls is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Show_Fouls unimplemented");
      raise Program_Error with "Unimplemented procedure Show_Fouls";
   end Show_Fouls;

end Score.Display;
