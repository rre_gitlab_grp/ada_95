------------------------Test_Score -------------------------
--  This test procedure sends a sequence of messages to the
--  Score.Events package involving shots made and fouls
--  committed, and messages to the Score.Display package
--  requesting display of data.
------------------------------------------------------------
with Score.Events;
with Score.Display;
use  Score;
procedure Test_Score is
begin
  Score.Display.Show_Scores;
  Score.Display.Show_Fouls;
  Score.Events.Shot_Made     (By => Bill, Shot => Two_Pointer);
  Score.Events.Foul_Committed (By => Carl);
  Score.Events.Shot_Made     (By => Hank, Shot => Foul_Shot);
  Score.Events.Shot_Made     (By => Dave, Shot => Three_Pointer);
  Score.Events.Shot_Made     (By => Fred, Shot => Two_Pointer);
  Score.Events.Foul_Committed (By => Glen);
  Score.Events.Shot_Made     (By => Eric, Shot => Foul_Shot);
  Score.Display.Show_Scores;
  Score.Display.Show_Fouls;
end Test_Score;
------------------------------------------------------------
