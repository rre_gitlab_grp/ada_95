------------------------------------------------------------
-------------------------- Score ---------------------------
--  This parent library package exports two enumeration
--  types and provides several private types, which are
--  accessible by its children. It needs no body.
------------------------------------------------------------

package Score is
  type Player_Name is (Adam, Bill, Carl, Dave, Eric, Fred, Glen, Hank, Ivan, John);
  type Shot_Type is (Foul_Shot, Two_Pointer, Three_Pointer);
  procedure Dispaly_All_Private;
  --procedure Display_Game_Name
private
  Home_Score     : Natural := 0;
  Visitors_Score : Natural := 0;

  Foul_Status    : array (Player_Name'Range) of Natural := (Player_Name'Range => 0);

  subtype Home_Player     is Player_Name range Adam .. Eric;
  subtype Visiting_Player is Player_Name range Fred .. John;
end Score;
