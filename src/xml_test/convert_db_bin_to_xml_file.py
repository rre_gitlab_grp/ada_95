"""
Author : RajaReddy
Date : 24May2021
Scope : This module is intended to convert binary_xx.xml to binary_xx.bin  recursively in current and subdirectories
    reference paths used are
    os.chdir('../../../../')
    genxml_pl = os.getcwd() + r"\native_tools\database_tools\GenXML\GenXML.pl"
    define_version = os.getcwd() + r"\appli_sw\skeleton\define_version.bat"
    static_txt = os.getcwd() + r"\appli_sw\skeleton\Static_"+current_static_ver+".txt"

"""
import re
import pathlib
import subprocess
import os
from multiprocessing import Pool
import datetime

current_static_ver = ''
static_txt = ''
genxml_pl = ''

# End of parseXML

def multi_process_func(path_command):
    name_list = path_command.split(';')
    os.chdir(pathlib.Path(name_list[0]))
    subprocess.run(name_list[1], stdin=None, timeout=None, check=False, stderr=None, shell=True)

def get_paths_and_names( ):
    global current_static_ver
    global static_txt
    global genxml_pl
    # Current static version and path  and path for  GenXml.Pl
    scenario_dir = os.getcwd()
    # os.chdir('../../../../')
    genxml_pl = os.getcwd() + r"\GenXML.pl"
    print('genxml_pl : ',genxml_pl )
    define_version = os.getcwd() + r"\define_version.bat"

    with open(define_version) as file:
        for line in file:
            if re.search("CURRENT_STATIC", line):
                current_static_ver = line.split('=')[1].strip()
                static_txt = os.getcwd() + r"\Static_"+current_static_ver+".txt"
                print('static_txt : ', static_txt)

    os.chdir(scenario_dir)      # back to script directory

if __name__ == "__main__":
    """
     : Main function
    Main Loop for the xml 
    """
    path_command_list = []
    ct_start = datetime.datetime.now()

    #Get Static version genxml.pl and static paths
    get_paths_and_names()

    #Get list of all binary xml files from current and sub directories
    binary_bin = 'binary_'+current_static_ver+'.bin'
    print('binary_bin : ', binary_bin, ' \nscenario_dir :',pathlib.Path.cwd())
    all_bin_files = pathlib.Path.cwd().rglob(os.path.basename(binary_bin))  # generator
    file_lst = list(all_bin_files)  # list of file names as strings

    # list of files to commands to convert xml to bin
    for file in file_lst :
        xml_file = pathlib.Path(file).with_suffix('.xml')
        if os.path.exists(xml_file):
            os.remove(xml_file)
        # Create the string with folder path and command for binary xml to bin with ';' separated
        if os.path.exists(file):  # perl GenXML.pl   -XML2bin   -bin binary_XB.bin  -XML binary_XB.xml -s Static_XB.txt
            path_name = pathlib.Path(file).parent
            path_command = "{} ; perl {} -bin2XML -XML {} -bin {} -s {}".format(path_name, genxml_pl, os.path.basename(xml_file), os.path.basename(file), static_txt)
            path_command_list.append(path_command)
        else:
            print("xml_file does not exist")

    if len(path_command_list) > 0:
        with Pool(None) as p:  #maximum number of CPU cores will be used
            p.map(multi_process_func, path_command_list)
    else:
        print("No files are found")

    ct_end = datetime.datetime.now()
    print("\n execution time :-", ct_end - ct_start)

#End of Main