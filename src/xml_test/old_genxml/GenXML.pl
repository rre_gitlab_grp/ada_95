#!/usr/bin/perl
#########################################################################################
# NAME		: GenXML.pl								#
# AUTHOR	: Bastien CHILLON							#
# DATE		: 24/01/06								#
$VERSION = '0.1.2';
# USAGE         : GenXML.pl 	[-d|-h] -bin binaryFile -XML XMLFile -s StaticFile	#
#				[-bin2XML]|[-XML2bin]#
#########################################################################################
# Ce script permet de convertir un fichier binaire en format XML apr�s de la description#
# d'un fichier de static. Une DTD est �galement g�n�r� pour d�crire le squelette du	#
# fichier XML.										#
# Param�tre d'entr�e:									#
# - XMFFile: le nom du fichier XML � convertir.						#
# - binaryFile: le nom du fichier binaire � convertir.					#
# - StaticFile: le nom du fichier d�crivant la statique.				#
# Aide: GenXML.pl -h									#
#########################################################################################
#Historique										#
#- BC: 30/08/2004 : Ajout de la gestion des �num�r�s.					#
#- BC: 02/09/2004 : Modification des commentaires 					#
#- BC: 30/09/2004 : Modification de l'interface pour pouvoir ajouter ou non des		#
#		    commentaires par ligne de commande					#
#                   Am�lioration du retour �cran.					#
#                   Correction d'un probl�me dans le parsage du fichier XML.		#
#		    Gestion des champs � longueur variable (INFO).			#
#- BC: 24/01/2006 : Ajout de la gestion des Enum�r�s ASCII pour g�rer la static MW	#
use Data::Dumper;

###########################################
#Initialisation des constantes
$FichierBinaire = "RBC1_BSL3_SM4.bin";
$FichierStatic = "static_30c2.txt";
$Fichier_XML = "RBC1_BSL3_SM4.XML";
$Static_interprete = "static.pl";
$DEBUG = 0;
$Lecture = 1;		#Lecture =1 => conversion de bin en XML
$PutCommentInXML = 0;

###########################################
#Initialisation des chaines recherch�es
##
#Element de la structure de la statique
$S_ElementTerminal = '\[([0-9]+) bits*\][ ]*([A-Za-z0-9_]+)';			#$1 Nb Bits, $2 Identifiant
$S_FinStructure    = '^\s*!\s*([0-9]+)[>]+\s*End\s*of\s*([A-Za-z0-9_]+)';	#$1 Num�ro de la structure, $2 Nom de la structure
$S_DebutStructure  = '^\s*!\s*([0-9]+)[>]+\s*([A-Za-z0-9_]+)';			#$1 Num�ro de la structure, $2 Nom de la structure
$S_RepetitionNumeric = '\*Repetition\* ([0-9]+)\{([A-Za-z0-9_]+)}';		#$1 Nb Repetition, $2 Nom de la structure r�p�t�e
$S_Repetition        = '\*Repetition\* ([A-Za-z0-9_]+)\{([A-Za-z0-9_]+)}';	#$1 Identifieur de la r�p�tition, $2 Nom de la structure r�p�t�e
$S_LongueurVariable  = '\[variable number of bits\][ ]*([A-Za-z0-9_]+)';	#$1 Identifieur
$S_DebutGroup        = '^\s*!\s*Group\s*([0-9])';				#$1 Num�ro du groupe
$S_DebutSousGroup    = '^\s*!\s*Subgroup\s*Header\s*Level\s*([0-9]+)';		#$1 Num�ro de sous-groupe

#D�tail des �l�ments terminaux de la statique
$S_SimpleItem_NonChar = $S_ElementTerminal.'\s*is\s*a\s*SIMPLE_ITEM\s*Range\s*=\s*([A-Za-z0-9_\-\.]+)\.\.([A-Za-z0-9\_\-\.]+).*Resolution\s*=\s*([0-9.-]+).*Units\s*=\s*([A-Za-z0-9_]+)';
							#$1 Nb Bits, $2 Identifiant, $3 RangeMin, $4 RangeMax, $5 Resolution, $6 Unit
$S_SimpleItem_Char    = $S_ElementTerminal.'.*ASCII_character.*Range=([A-Za-z0-9]+).*';
							#$1 Nb Bits, $2 Identifiant, $3 Range
$S_Constant           = $S_ElementTerminal.'\s*is\s*a\s*CONSTANT\s*VALUE\s*=\s*([0-9]+)';
							#$1 Nb Bits, $2 Identifiant, $3 Constant
$S_Enumerate          = $S_ElementTerminal.'\s*is\s*a[n]*\s*ENUMERATION\s*:\s*([A-Za-z0-9_|]+)';
							#$1 Nb Bits, $2 Identifiant, $3 Liste des �num�r�s
$S_ASCII_Enumerate    = $S_ElementTerminal.'.*ASCII_ENUMERATION.*\s*:\s*([A-Za-z0-9_|]+)';			#BC_25012006
							#$1 Nb Bits, $2 Identifiant, $3 Liste des �num�r�s	#BC_25012006						

#Element du fichier XML
$X_DebutStructure     = '<([A-Za-z0-9_]+)>';					#$1 Identifiant
$X_FinStructure       = '<\/([A-Za-z0-9_]+)>';					#$1 Identifiant
$X_Element            = $X_DebutStructure.'([A-Za-z0-9_ -.]+)<\/\1>';		#$1 Identifiant, $2 Valeur
$X_ElementVide        = '<([A-Za-z0-9_]+)\/>';					#$1 Identifiant
$X_Enumere            = '<([A-Za-z0-9_]+)\s+ENUM_VALUE=\"([A-Za-z0-9_]+)\s*\"\/>';	#$1 Identifiant, $2 Enum�r� BC_25012006

while($#ARGV >=0){
	$_ = shift(@ARGV);
	if    ( $_ =~ /^-d$/i)  {print "Passage en mode DEBUG\n"; $DEBUG = 1;}
	elsif ( $_ =~ /^-v$/i)  {print $VERSION; exit;}
	elsif ( $_ =~ /^-bin$/i){$FichierBinaire = shift(@ARGV);}
	elsif ( $_ =~ /^-XML$/i){$Fichier_XML = shift(@ARGV);}
	elsif ( $_ =~ /^-S$/i)  {$FichierStatic = shift(@ARGV);}
	elsif ( $_ =~ /^-bin2XML$/i){$Lecture = 1;}
	elsif ( $_ =~ /^-XML2bin$/i){$Lecture = 0;}
	elsif ( $_ =~ /^-comment$/i){$PutCommentInXML = 1;}
	else{	#Affichage de l'aide
		print "\nPURPOSE:\n";
		print "--------\n";

		print "GenXML.pl is a perl script used to convert RBC Database\n";
		print "from XML files to binary and conversely.\n\n";

		print "USAGE:\n";
		print "------\n";
		print "GenXML.pl [-d][-h][-v][-comment]\n";
		print "           [-bin BinaryFile]\n";
		print "           [-XML XMLFile]\n";
		print "           [-s StaticFile]\n";
		print "           [-bin2XML]|[-XML2bin]\n\n";

		print "-- OPTION --\n";
		print " -d       : Debug mode\n";
		print " -bin     : Indicates the binary file\n";
		print " -XML     : Indicate the RBC database save as XML file\n";
		print " -s       : Indicate the static database decription to be used\n";
		print " -bin2XML : Convert binary to XML file\n";
		print " -XML2bin : Convert XML file to binary\n";
		print " -comment : Display comments in the XML File\n";
		print " -v       : Diplay the version used\n";
		print " -h       : print help\n\n";

		print "\n[Appuyez sur RETURN pour continuer ...]\n" ;

		while(<STDIN>)
		{
			last;
		}
		exit -1;
 	}
}

print STDERR "\nExecution de GenXML\n";
print STDERR "Fichier de static: $FichierStatic\n";
print STDERR "RBC Database     : $Fichier_XML\n";
print STDERR "Binary file      : $FichierBinaire\n";
if(!$Lecture){print STDERR "Conversion XML ==> binary\n"}
else{print STDERR "Conversion binary ==> XML\n";}

## Generation de la DTD
$FichierDTD = substr($FichierStatic,0,-4).".dtd";
if (!(-f $FichierDTD)){GenereDTD($FichierDTD);}

###########################################
#Variable Globale
$Fichier_en_Binaire = "";
$Indice_Binaire = 0;
$NbLength = 0;
$InfoPresente = 0;
@TabLength = ();

###########################################
#Programme principal
LireStatic();
require "$Static_interprete";
if ($Lecture){
	open(XML,">$Fichier_XML")||TraiteErreur("Can not open $Fichier_XML for writing","ERROR");
	LireBinaire();
	Eval_Static();
	close(XML);
}else{
	CheckXML();
	@XML_Parse = ();
	ParseXML();
	#Calcul des longueurs
	$LengthComputation = 1;
	Eval_Static();
	
	#Ecriture des longueurs dans le fichier resultant
	$IndiceParse = 0;
	open(XML, "$Fichier_XML")||TraiteErreur("Can not open $Fichier_XML","ERROR");
	open(TEMP, ">$Fichier_XML.tmp")||TraiteErreur("Can not open $Fichier_XML.tmp","ERROR");
	$IndiceLength = 0;
	while(<XML>){
		if(/<LENGTH>/i){s/(<LENGTH>)[0-9]+(<\/LENGTH>)/$1$TabLength[$IndiceLength++]$2/g;}
		print TEMP;
	}
	close(TEMP);
	close(XML);

	rename("$Fichier_XML\.tmp",$Fichier_XML);

	#Ecriture du resultat dans le binaire
	$LengthComputation = 0;
	$IndiceParse = 0;
	$IndiceLength = 0;
	@XML_Parse = ();
	ParseXML();
	Eval_Static();
	open(BIN, ">$FichierBinaire")||TraiteErreur("Can not open $FichierBinaire","ERROR");
	binmode(BIN);
	print BIN bin2String($Fichier_en_Binaire);
	close(BIN);
}
unlink("$Static_interprete");

###########################################
#Proc�dure relative � la lecture des fichiers
sub LireBinaire{
	open(FICBIN, $FichierBinaire)||TraiteErreur("Can not open $FichierBinaire file\n","ERROR");;
	binmode(FICBIN);
	$Fichier_en_Binaire = Ascii2Bin(LireFichier(FICBIN));
	close(FICBIN);
}

sub LireFichier{
	($FileHandle) = @_;
	my $Octet, $Contenu;
	while (not eof($FileHandle)){
		read($FileHandle,$Octet,1);
		$Contenu .= $Octet;
	}

	return $Contenu;
}

sub Ascii2Bin{
	my($Chaine) = @_;
	my $j;
	my $Bin;
	my $ChaineInterpretee;
	for($j=0;$j<length($Chaine);$j+=4){
		$ChaineInterpretee .= unpack("B32",substr($Chaine,$j,4));
	}
	return $ChaineInterpretee;
}

sub ParseXML{
	open(XML, "$Fichier_XML")||TraiteErreur("Can not open file $Fichier_XML","ERROR");

	while (<XML>){
		if (/$X_Element/){push @XML_Parse, "$2\t$1";if($1 eq "INFO"){$InfoPresente = 1;}}
		elsif (/$X_ElementVide/){push @XML_Parse, "\t$1";}
		elsif (/$X_Enumere/){push @XML_Parse, "$2\t$1";}
	}

	close(XML);
}

###########################################
#Proc�dure relative � la g�n�ration d'une DTD
sub GenereDTD{
	my($FichierDTD) = @_;
	my %HashElement = ();
	my $Level = 0;
	my %HashStruct = ();
	my @NameOfLevel = ();
	my %RepetedStruct = ();
	my %FinishedStruct = ();
	my %EnumListe = ();
	my $ElementName;

	$NameOfLevel[0] = 'Database';
	$HashStruct{$NameOfLevel[0]} = '';

	print "Fichier DTD: $FichierDTD\n" if $DEBUG;
	open(STATIC, $FichierStatic)||TraiteErreur("Can not open $FichierStatic file","ERROR");
	while(<STATIC>){
		s/^\s*//g;
		if(/$S_ElementTerminal/i){
			$ElementName = $2;
			#D�finition des �l�ments finaux
			#Si on trouve un �num�r�, on le note dans EnumListe
			#Sinon on le note dans la liste des �l�ments terminaux
			if(/$S_Enumerate/i){
				$EnumListe{$ElementName} = $3;
			}
			elsif(/$S_ASCII_Enumerate/i){			#BC_25012006
				$EnumListe{$ElementName} = $3;		#BC_25012006
			}						#BC_25012006
				
			else{
				$HashElement{$ElementName} = 1;
			}

			#D�finition des �l�ments non terminaux
			if (!$FinishedStruct{$NameOfLevel[$Level]}){
				if($HashStruct{$NameOfLevel[$Level]}){
					$HashStruct{$NameOfLevel[$Level]} .= ",$ElementName";
				}else{
					$HashStruct{$NameOfLevel[$Level]} = $ElementName;
				}
				if($RepetedStruct{$ElementName}){$HashStruct{$NameOfLevel[$Level]} .= '*';}
			}
		}
		elsif(/$S_FinStructure/i){
			#Une fin de structure est trouv�e, on repasse � la structure de niveau
			#sup�rieure
			$Level--;
			$FinishedStruct{$2} = "Finished";
		}
		elsif(/$S_DebutStructure/i){
		#On vient de trouver un d�but de structure
			if($HashStruct{$NameOfLevel[$Level]}){
				#Si la structure parente existe d�j�, ajout dans la structure parente
				#de la nouvelle structure
				$HashStruct{$NameOfLevel[$Level]} .= ",$2";

				#Si cette structure peut �tre r�p�t�e ajout d'un *
				if($RepetedStruct{$2}){$HashStruct{$NameOfLevel[$Level]} .= '*';}
			}else{
				#Si la structure parente n'existe pas, cr�ation de la structure parente
				#avec le nom de la nouvelle structure
				$HashStruct{$NameOfLevel[$Level]} = $2;

				#Si cette structure peut �tre r�p�t�e ajout d'un *
				if($RepetedStruct{$2}){$HashStruct{$NameOfLevel[$Level]} .= '*';}
			}
			#Le nom de la structure de plus haut niveau est celui de la nouvelle structure
			$NameOfLevel[++$Level] = $2;
		}
		elsif(/$S_RepetitionNumeric/i){
			$RepetedStruct{$2}.= $NameOfLevel[$Level];
		}
		elsif(/$S_Repetition/i){
			$RepetedStruct{$2}.= $NameOfLevel[$Level];
		}
		elsif(/$S_LongueurVariable/i){
			$HashStruct{$NameOfLevel[$Level]} .= ",INFO?";
			$HashElement{"INFO"} = 1;
		}
		else{
			s/!.*//g;
			s/^\s*//g;
			if(!/^$/){
				close(OUT);
				unlink("$Static_interprete");
				TraiteErreur("In $FichierStatic, the line\n$_ seems not correct","ERROR");
			}
		}
	}
	close(STATIC);

	#Ecriture dans le fichier r�sultant
	open(DTD, ">$FichierDTD")||TraiteErreur("Can not open $FichierDTD","ERROR");

	print DTD "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	foreach $key (keys %HashElement){
		print DTD "<!ELEMENT $key (#PCDATA)>\n";
	}
	foreach $key (keys %EnumListe){
		print DTD "<!ELEMENT $key EMPTY>\n<!ATTLIST $key ENUM_VALUE ($EnumListe{$key}) #REQUIRED>\n";
	}
	foreach $key (keys %HashStruct){
		print DTD"<!ELEMENT $key ($HashStruct{$key})>\n";
	}
	close(DTD);
}

###########################################
#Proc�dure relative � la lecture du fichier de static
sub LireStatic{
	my @Repetition =();
	my $Blanc = "  ";
	my $Ligne;
	my $Iterateur=0;
	my $NbBits;
	open(STATIC, $FichierStatic)||TraiteErreur("Can not open $FichierStatic file","ERROR");
	open(OUT, ">$Static_interprete")||TraiteErreur("Can not open $Static_interprete","ERROR");
	print OUT "sub Eval_Static{\n";

	print OUT "${Blanc}print XML \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\"?>\\n\";\n";
	print OUT "${Blanc}print XML \"<!DOCTYPE Database SYSTEM \\\"$FichierDTD\\\">\\n<Database>\\n\";\n";
	while(<STATIC>){
		s/^\s*//g;
		if(/$S_ElementTerminal/i){
			chomp($_);
			$NbBits = $1;
			if(/SIMPLE_ITEM/i){
				print OUT "${Blanc}Traite_Simple_Item($NbBits,\"$_\");\n";
			}
			elsif(/$S_Constant/i){
				print OUT "${Blanc}Traite_Constant($NbBits,\"$_\");\n";
			}
			elsif(/$S_Enumerate/i){
				print OUT "${Blanc}Traite_Enumeration($NbBits,\"$_\");\n";
			}
			else{
				print OUT "${Blanc}Traite_Integer($NbBits,\"$_\");\n";
				if(/LENGTH/){print OUT "$Blanc\$IndiceLevel\[\$Level\] = \$NbLength++;\n";}
			}
		}
		elsif(/$S_RepetitionNumeric/i){
			push(@Repetition, $2);
			print OUT "${Blanc}for(\$j${Iterateur}=1;\$j${Iterateur}<=$1;\$j${Iterateur}++){\n";
			$Blanc .= "  ";
			$Iterateur++;
		}
		elsif(/$S_Repetition/i){
			push(@Repetition, $2);
			print OUT "${Blanc}for(\$j${Iterateur}=1;\$j${Iterateur}<=\$Hash{\"$1\"};\$j${Iterateur}++){\n";
			$Blanc .= "  ";
			$Iterateur++;
		}
		elsif(/$S_DebutGroup/i){
			print OUT "$Blanc\$Level = 0; \$IndiceLevel\[\$Level\]=\"\";\n";
		}
		elsif(/$S_DebutSousGroup/i){
			print OUT "$Blanc\$Level = $1; \$IndiceLevel\[\$Level\]=\"\";\n";
		}
		#Si il s'agit du premier groupe, on g�n�re la structure XML
		#Sinon on prend la structure donn�e par le 0X>>>
		elsif(/$S_FinStructure/i){
			print OUT "${Blanc}print XML \"</$2>\\n\";\n";
		}
		elsif(/$S_DebutStructure/i){
			print OUT "${Blanc}print XML \"<$2>\\n\";\n";
		}
		elsif(/$S_LongueurVariable/i){
			print OUT "${Blanc}Traite_INFO();\n";
		}
		else{
			s/!.*//g;
			s/^\s*//g;
			if(!/^$/){
				close(OUT);
				unlink("$Static_interprete");
				TraiteErreur("In $FichierStatic, the line\n$_ seems not correct","ERROR");
			}
		}
		if(/End of \b($Repetition[$#Repetition])\b/i or /\[[0-9]+ bits*\]$Repetition[$#Repetition] is /i){
			if($Repetition[$#Repetition] ne ""){
				$Blanc = substr($Blanc,2);
				print OUT "${Blanc}}\n";
				$Iterateur--;
			}
			pop @Repetition;
		}
	}

	print OUT "${Blanc}print XML \"</Database>\";\n";
	print OUT "}\n1;";
	close(OUT);
	close(STATIC);
}


#Param0: Nombre de bits � lire
#Param1: Ligne extraite de la d�finition du static
sub Traite_Simple_Item{
	my ($BitaLire, $Ligne) = @_;
	my $Value, @Param;

	print "Traitement de Simple_Item\n" if $DEBUG;
	if($Lecture){
		if ($Ligne =~ /$S_SimpleItem_NonChar/){
			$Value = bin2dec(getNbits($BitaLire))*$5 + $3;
			$Hash{$2} = $Value;
			chomp($Ligne);
			print XML "<$2>$Value</$2>\n";
			print XML "<!-- Range=$3..$4 Resolution=$5 Units=$6 -->\n" if $PutCommentInXML;
		}
		elsif($Ligne =~ /$S_SimpleItem_Char/i){
			$Value = bin2String(getNbits($BitaLire));
			chomp($Ligne);
			print XML "<$2>$Value</$2>\n";
 			print XML "<!-- $3 -->\n" if $PutCommentInXML;;
		}
		elsif($Ligne =~ /$S_ASCII_Enumerate/i){			#BC_25012006
			$Value = bin2String(getNbits($BitaLire));	#BC_25012006
			chomp($Ligne);					#BC_25012006
			$Param_Temp = $2;					#BC_25012006
			$Param_Temp1 = $3;
			$Value =~ s/\s+//;				#BC_25012006
			print XML "<$Param_Temp ENUM_VALUE=\"$Value\"/>\n";#BC_25012006
 			print XML "<!-- $Param_Temp1 -->\n" if $PutCommentInXML;	#BC_25012006
		}
		elsif($Ligne =~ /ASCII_character/){
			$Value = bin2String(getNbits($BitaLire));
			chomp($Ligne);
			print XML "<ID>$Value</ID>\n";
			print XML "<!-- ASCII_character -->\n" if $PutCommentInXML;;
		}
		else{
			getNbits($BitaLire);
			TraiteErreur("The line\n\t\t$Ligne\n\t\tcan not be interpreted\n","WARNING");
		}
	}else{
		if(!$LengthComputation){
			@Param = split(/\t/,GetLine());
			if ($Ligne =~ /$S_SimpleItem_NonChar/){
			#	$NomVariable = $2; $RangeMin = $3; $RangeMax = $4;
			#	$Resolution = $5; $Units = $6;
				$Param[1] eq $2
				|| TraiteErreur("The parameter $2 is waited while $Param[1] found, check your Nmax parameter","ERROR");

				if($Hash{$4}){$Value = $Hash{$4};}else{$Value = $4;}
 				if($Param[0]  < $3){
					TraiteErreur("The Value $Param[0] for\n\t\t$Ligne\n\t\t is not in the range $3..$4 (=$Value) too small","ERROR");
				}
 				elsif($Param[0]  > $Value){
					TraiteErreur("The Value $Param[0] for\n\t\t$Ligne\n\t\t is not in the range $3..$4 (=$Value) too big","ERROR");
				}
				$Hash{$2} = $Param[0];
				$Fichier_en_Binaire .= dec2bin(($Param[0] - $3)/$5, $BitaLire);
				print dec2bin(($Param[0] - $3)/$5, $BitaLire)."\n" if $DEBUG;

				#V�rification des N par rapport aux Nmax
				my $Var = $2;
				if(($Var =~ /^N_(.*)/i) and ($Hash{"Nmax_$1"} ne "")){
					if($Hash{$Var} > $Hash{"Nmax_$1"}){TraiteErreur("The value $Var (= $Hash{$Var}) shall smaller or equal to Nmax_$1 (= $Hash{\"Nmax_$1\"})","WARNING")};
				}
			}
			elsif($Ligne =~ /$S_SimpleItem_Char/i){
				$Param[1] eq $2
				|| TraiteErreur("The parameter $2 is waited while $Param[1] found, check your Nmax parameter","ERROR");
				$Fichier_en_Binaire .= String2bin(substr($Param[0].(" " x ($BitaLire/8)),0,($BitaLire/8)),$BitaLire);
				print String2bin($Param[0],$BitaLire)."\n" if $DEBUG;
			}
			elsif($Ligne =~ /$S_ASCII_Enumerate/i){												#BC_25012006
				$Param[1] eq $2
				|| TraiteErreur("The parameter $2 is waited while $Param[1] found, check your Nmax parameter","ERROR");			#BC_25012006
				$Fichier_en_Binaire .= String2bin(substr($Param[0].(" " x ($BitaLire/8)),0,($BitaLire/8)),$BitaLire);			#BC_25012006
				print String2bin($Param[0],$BitaLire)."\n" if $DEBUG;									#BC_25012006
			}																#BC_25012006
			elsif($Ligne =~ /ASCII_character/){
				$Param[1] eq "ID"
				|| TraiteErreur("The parameter ID is waited while $Param[1] found, check your Nmax parameter","ERROR");
				$Fichier_en_Binaire .= String2bin(substr($Param[0].(" " x ($BitaLire/8)),0,($BitaLire/8)),$BitaLire);
				print String2bin($Param[0],$BitaLire)."\n" if $DEBUG;
			}
			else{
				TraiteErreur("The line\n\t\t$Ligne\n\t\tmight not be coded correctly","WARNING");
				$Fichier_en_Binaire .= String2bin(substr((" " x ($BitaLire/8)),0,($BitaLire/8)),$BitaLire);
				print String2bin($Param[0],$BitaLire)."\n" if $DEBUG;
			}
		}
		else{
			@Param = split(/\t/,GetLine());
			if ($Ligne =~ /$S_SimpleItem_NonChar/i){
				$Hash{$2} = $Param[0];
			}
			ManageLength($BitaLire, $Ligne);
		}
	}
}

#Param0: NbBits
#Param1: Ligne extraite de la d�finition du static
sub Traite_Constant{
	my @Param = @_;
	print "Traitement de Constant\n" if $DEBUG;
	if($Lecture){
		my $Value = bin2dec(getNbits($Param[0]));

		if ($Param[1] =~ /$S_Constant/i){
			($Value == $3)
			||TraiteErreur("The constant $Param[1]\n\t\tfound set to $Value\n\t\tThe resulted file might not be correct\n","WARNING");
			print XML "<$2>$3</$2>\n";
		}
	}else{
		if(!$LengthComputation){
			my @Ligne = split(/\t/,GetLine());

			#V�rification que l'on traite la bonne ligne
			$Param[1] =~ /$S_Constant/ && $Ligne[1] eq $2
			|| TraiteErreur("The parameter $2 (=$Param[1]) is waited while $Ligne[1] found, check your Nmax parameter","ERROR");

			#V�rification que la valeur correspond � la constante
			($Ligne[0] == $3)
			||TraiteErreur("The constant $Param[1]\n\t\tfound set to $Value\n\t\tThe resulted file might not be correct\n","WARNING");

			#Traitement de la donn�e
			$Fichier_en_Binaire .= dec2bin($3,$Param[0]);
		}
		else{
			GetLine();
			ManageLength(@Param);
		}
	}
}

#Param0: NbBits
#Param1: Ligne extraite de la d�finition du static
sub Traite_Enumeration{
	my (@Param) = @_;

	print "Traitement de Enumeration\n" if $DEBUG;
	if($Lecture){
		my $Value = bin2dec(getNbits($Param[0]));
		if ($Param[1] =~ /$S_Enumerate/i){
			my @Enum = split(/\|/,$3);
			print XML "<$2 ENUM_VALUE=\"$Enum[$Value]\"/>\n";
		}else{
			TraiteErreur("The ligne $Param[1] can not be interpreted as an ENUMERATE\n","ERROR");
		}
	}else{
		if(!$LengthComputation){
			my @Ligne = split(/\t/,GetLine());
			$Param[1] =~ /$S_Enumerate/i && $Ligne[1] eq $2
			|| TraiteErreur("The parameter $2 is waited while $Ligne[1] found, check your Nmax parameter","ERROR");
			$Fichier_en_Binaire .= dec2bin(getEnumValue($Ligne[0],$3),$Param[0]);
			print dec2bin($Ligne[0],$Param[0])."\n" if $DEBUG;
		}
		else{
			GetLine();
			ManageLength(@Param);
		}
	}
}

#Param0: NbBits
#Param1: Ligne extraite de la d�finition du static
sub Traite_Integer{
	my @Param = @_;

	print "Traitement de Integer\n" if $DEBUG;
	if($Lecture){
		my $Value = bin2dec(getNbits($Param[0]));
		if ($Param[1] =~ /$S_ElementTerminal/i){
			print XML "<$2>$Value</$2>\n";
			chomp($Param[1]);
			if($2 =~ /LENGTH/i){$LengthCourante = $Value;}
		}
	}else{
		if(!$LengthComputation){
			my @Ligne = split(/\t/,GetLine());
			$Param[1] =~ /$S_ElementTerminal/i && ($Ligne[1] eq $2)
			|| TraiteErreur("The parameter $2 is waited while $Ligne[1] found, check your Nmax parameter","ERROR");
			$Fichier_en_Binaire .= dec2bin($Ligne[0],$Param[0]);
			print dec2bin($Ligne[0],$Param[0])."\n" if $DEBUG;
		}
		else{
			GetLine();
			ManageLength(@Param);
		}
		if ($Param[1] =~ /\[[0-9]+ bits*\][ ]*length/i){$LengthCourante = $Ligne[0];}
	}
}

sub Traite_INFO{
	my $NbBits = $LengthCourante - 32;
	my $Value;
	my @Param;
	if($NbBits != 0){
		if($Lecture){
			$Value = bin2String(getNbits($NbBits));
			print XML "<INFO>$Value</INFO>\n";
			print XML "<!-- ASCII_character -->\n" if $PutCommentInXML;
		}
		else{
			if(!$LengthComputation and $InfoPresente){
				@Param = split(/\t/,GetLine());
				$Fichier_en_Binaire .= String2bin($Param[0],length($Param[0])*8);
				print String2bin($Param[0],length($Param[0]))."\n" if $DEBUG;
			}
		}
	}
	if($LengthComputation and $InfoPresente and !$Lecture){
		@Param = split(/\t/,GetLine());
		ManageLength(length($Param[0])*8);
	}
}

#Param0: NbBits
#Param1: Ligne extraite de la d�finition du static
sub ManageLength{
	my(@Param) = @_;
	my $IndLevel;

	if($Param[1] =~ /CRC/i){
		$TabLength[$IndiceLevel[0]] += $Param[0];
	}else{
		for($IndLevel = 0;$IndLevel <= $Level;$IndLevel++){
			if ($IndiceLevel[$IndLevel] eq ""){last;}
			$TabLength[$IndiceLevel[$IndLevel]] += $Param[0];
		}
	}
}

sub getNbits{
	my($Nb) = @_;
	$Indice_Binaire += $Nb;
	return substr($Fichier_en_Binaire,$Indice_Binaire-$Nb,$Nb);
}

#Retourne une valeur d�cimal sur elle est cod�e sur 32 bits au plus
#ou une valeur en hexa si elle est cod�e sur plus de 32 bits
sub bin2dec{
	my $Value = shift;
	my $Longueur = length($Value);
	if($Longueur<=32){
		return unpack("N", pack("B32", substr("0" x 32 . $Value, -32)));
	}else{
		return unpack("H$Longueur", pack("B$Longueur", substr("0" x $Longueur . $Value, -$Longueur)))."h";
	}
}

#Param0: Valeur � transfomer en binaire
#Param1: Nombre de bits sur lesquels l'information est cod�e
sub dec2bin{
	my($Number, $NbBits) = @_;
	if($Number =~ /h/){	#Valeur en hexa
		$Number = substr($Number,0,length($Number)-1);
		my $Longueur = length($Number)*4;
		substr(unpack("B$Longueur",pack("H$Longueur",$Number)),-$NbBits);
	}
	else{
		substr(unpack("B32",pack("N",$Number)),-$NbBits);
	}
}

sub bin2char{
	chr(bin2dec(shift));
}

sub bin2String{
	my ($Binaire) = @_;
	my $Chaine = "";
	my $k;
	for($k=0;$k<length($Binaire);$k+=8){
		$Chaine .= bin2char(substr($Binaire,$k,8));
	}
	return $Chaine;
}

sub String2bin{
	my ($String) = @_;
	my $Chaine = "";
	my $k;
	for($k=0;$k<length($String);$k++){
		$Chaine .= unpack("B8",unpack("a", substr($String,$k,1)));
	}
	return $Chaine;
}

sub getEnumValue{
	my ($Enum, $Liste) = @_;
	my $k=0;
	my @Liste_tab = split(/\|/, $Liste);
	for($k=0; $k<=$#Liste_tab; $k++){
		if($Enum eq $Liste_tab[$k]){return $k;}
	}
}

sub GetLine{
	return $XML_Parse[$IndiceParse++];
}

sub TraiteErreur{
   my($TextErreur,$KIND) = @_;
   if($KIND eq "ERROR"){
	print "ERROR:\t\t$TextErreur\n";
   	exit(-1);
   }
   elsif($KIND eq "WARNING"){
   	print "WARNING:\t$TextErreur\n";
   }
}

sub CheckXML{
	my $Structure = "/Database";
	my %Repetition;
	my %Repeteur;
	my %NbRepetition;
	my $Value2Checked;
	my $Element;

	sub CompteElement{
		my($Elem2Count) = @_;
		if($Repetition{$Structure}{$Elem2Count}){
			$NbRepetition{$Structure}{$Elem2Count}++;
		}
	}

	open(STATIC, $FichierStatic)||TraiteErreur("Can not open $FichierStatic file","ERROR");
	while(<STATIC>){
		s/^\s*//g;
		if(/$S_Repetition/i){
			$Repetition{$Structure}{$2} = $1;
			$Repeteur{$1} = 1;
		}
		elsif(/$S_FinStructure/i){
			$Structure =~ s/\/[A-Za-z0-9_]+$//g;
		}
		elsif(/$S_DebutStructure/i){
			$Structure .= "/$2";
		}
	}

	close(STATIC);

	$Structure = "";
	open(XML, "$Fichier_XML")||TraiteErreur("Can not open file $Fichier_XML","ERROR");
	while (<XML>){
		if (/$X_Element/){
			if($Repeteur{$1} ne ""){$Repeteur{$1} = $2;}
			else{CompteElement($1)};
		}
		elsif (/$X_ElementVide/){CompteElement($1);}
		elsif (/$X_DebutStructure/){
			CompteElement($1);
			$Structure .= "/$1";
		}
		elsif (/$X_Enumere/){
			CompteElement($1);
		}
		elsif (/$X_FinStructure/){
			if($Repetition{$Structure}){
				foreach $Element (keys(%{$Repetition{$Structure}})){
					#Si le r�p�teur est un chiffre fixe dans la static, on v�rifie par rapport � ce chiffre
					#Sinon on v�rifie par rapport � la valeur trouv�e dans le fichier XML
					if($Repetition{$Structure}{$Element} =~ /^[0-9]+$/){$Value2Checked = $Repetition{$Structure}{$Element};}
					else{$Value2Checked = $Repeteur{$Repetition{$Structure}{$Element}};}

					if($NbRepetition{$Structure}{$Element} == $Value2Checked){
						$NbRepetition{$Structure}{$Element} =0;
					}
					else{
						TraiteErreur("In the structure $Structure,\n\t\t there are $NbRepetition{$Structure}{$Element} $Element while $Repetition{$Structure}{$Element} is set to $Repeteur{$Repetition{$Structure}{$Element}}","ERROR");
						$NbRepetition{$Structure}{$Element} =0;
					}
				}
			}
			$Structure =~ s/\/[A-Za-z0-9_]+$//g;
		}
	}

	close(XML);
}