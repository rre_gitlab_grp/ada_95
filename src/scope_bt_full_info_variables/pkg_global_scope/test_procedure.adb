with Ada.Text_IO; use Ada.Text_IO;
with Week;
use Week;

procedure Test_Procedure is

  type Weekend_Days is new Days range Saturday .. Sunday;
  -- A procedure Print_Day is automatically inherited here. It is as if
  -- the procedure
  -- procedure Print_Day (D : Weekend_Days);
  -- has been declared with the same body
  Sat : Weekend_Days := Saturday;
begin
  Print_Day (Sat);
  Print_Day (Global_Data_Ads);  --try gdb Global_Data_Ads'size
  Print_Day (Global_Data_Ads_Private);  -- pkg body globals are not visible to clients.
end Test_Procedure;
