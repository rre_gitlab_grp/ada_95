package Week is
  type Days is (Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday);
  -- Print day is a primitive of the type Days
  procedure Print_Day (D : Days);
  Global_Data_Ads : Days := Monday;
private
  Global_Data_Ads_Private : Days := Wednesday;

end Week;
