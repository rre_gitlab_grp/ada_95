with Ada.Text_IO; use Ada.Text_IO;
package body Week is
  Global_Data_Adb : Days := Wednesday;
  procedure Print_Day (D : Days) is
  begin
    Global_Data_Ads := Tuesday;
    Global_Data_Adb := Friday;
    Put_Line (Days'Image (D));
    Put_Line (Days'Image (Global_Data_Adb));
  end Print_Day;
begin
  Put_Line ("Elaboration: Essentially execution of declaration");
  -- place a break point and see call-stack or F5, F6
end Week;
