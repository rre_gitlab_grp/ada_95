with My_Text_Io;
with Satellite;
use type Satellite.Real_T;
procedure Pretty_Print
  (Prompt : String;
   Data   : Satellite.Data_T) is
   -- FPS = (Miles/Hour) * (1 Hour / 60 Minutes) * ( 1 Minute / 60 seconds) * (
   -- 5280 feet / 1 mile )
   Hour_To_Minutes   : constant := 1.0 / 60.0;
   Minute_To_Seconds : constant := 1.0 / 60.0;
   Feet_To_Mile      : constant := 5280.0 / 1.0;
   Fps               : Satellite.Real_T;
begin
   My_Text_Io.Put_Line (Prompt);
   My_Text_Io.Put_Line
     ("   Number_Of_Orbits => " & Satellite.Real_T'Image(Data.Number_Of_Orbits));
   My_Text_Io.Put_Line ("   Time_Orbiting    => " & Satellite.Real_T'Image(Data.Time_Orbiting));
   My_Text_Io.Put_Line ("   Altitude         => " & Satellite.Real_T'Image(Data.Altitude));
   Fps := Data.Velocity * Hour_To_Minutes * Minute_To_Seconds * Feet_To_Mile;
   My_Text_Io.Put_Line ("   Velocity         => " & Satellite.Real_T'Image(Fps) & " fps");
end Pretty_Print;
