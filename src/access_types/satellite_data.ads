with Satellite;
package Satellite_Data is

   type Space_Agencies_T is (Nasa, Esa, Rsa);

   -- initialize with "valid" values
   Database : array (Space_Agencies_T) of Satellite.Data_T := (others => null);

end Satellite_Data;
