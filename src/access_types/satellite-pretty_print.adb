with My_Text_Io;
package body Satellite.Pretty_Print is
   procedure Print
     (Prompt : String;
      Data   : Data_T) is
   begin
      My_Text_Io.Put_Line (Prompt);
      My_Text_Io.Put_Line
        ("   Number_Of_Orbits => " & Real_T'Image(Data.Number_Of_Orbits));
      My_Text_Io.Put_Line
        ("   Time_Orbiting    => " & Real_T'Image(Data.Time_Orbiting));
      My_Text_Io.Put_Line ("   Altitude         => " & Real_T'Image(Data.Altitude));
      My_Text_Io.Put_Line ("   Velocity         => " & Real_T'Image(Data.Velocity));
   end Print;
end Satellite.Pretty_Print;
