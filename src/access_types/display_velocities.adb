with My_Text_Io;
with Satellite;
with Satellite_Data; use Satellite_Data;
with Satellite.Pretty_Print;
with Pretty_Print;

procedure Display_Velocities is

  procedure Update_Input_Value
   (Prompt :        String;
    Value  : in out Satellite.Real_T) is
    Input : String (1 .. 20);
    Last  : Integer range Input'First - 1 .. Input'Last;
  begin
    loop
      My_Text_Io.Put
       ("Enter " & Prompt & " (return to use" & Satellite.Real_T'Image (Value) & "): ");
      My_Text_Io.Get_Line (Input, Last);
      exit when Last = Input'First - 1;
      Value := Satellite.Real_T'Value (Input (Input'First .. Last));
    end loop;
  end Update_Input_Value;

  Temporary_Data : aliased Satellite.Data_Record_T := (others => 0.0);

begin
  for Agency in Space_Agencies_T
  loop
    -- get values
    Update_Input_Value (Space_Agencies_T'Image (Agency) & " altitude", Temporary_Data.Altitude);
    Update_Input_Value
     (Space_Agencies_T'Image (Agency) & " number_of orbits", Temporary_Data.Number_Of_Orbits);
    Update_Input_Value
     (Space_Agencies_T'Image (Agency) & " time orbiting", Temporary_Data.Time_Orbiting);
    if Satellite.Input_Values_In_Range (Temporary_Data'Unchecked_Access)
    then
      Satellite_Data.Database (Agency) := new Satellite.Data_Record_T'(Temporary_Data);
    end if;
  end loop;

  for Agency in Space_Agencies_T
  loop
    -- display values
    My_Text_Io.Put (Space_Agencies_T'Image (Agency) & " => ");
    Database (Agency).Velocity := Satellite.Velocity (Database (Agency));
    My_Text_Io.Put_Line
     ("Velocity = " & Satellite.Velocity (Database (Agency)));
  end loop;

  for  Agency in Space_Agencies_T
  loop
    if Database (Agency).Velocity not in Satellite.Velocity_Range_T
    then
      My_Text_Io.Put_Line ("A velocity is out of range");
    end if;
  end loop;

  for Agency in Space_Agencies_T
  loop
    Satellite.Pretty_Print.Print
     ("Debug " & Space_Agencies_T'Image (Agency), Database (Agency));
    Pretty_Print ("Feet Per Second " & Space_Agencies_T'Image (Agency), Database (Agency));
  end loop;
end Display_Velocities;
