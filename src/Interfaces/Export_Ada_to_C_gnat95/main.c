#include <stdio.h>
#include "c_struct.h"
extern int my_func (int a);
extern int func_cnt;
extern struct c_struct c_record;

int main (int argc, char **argv)
{
  int v;
  v = my_func(1);
  v = my_func(2);
  v = my_func(3);
  printf("Result is %d\n", v);
  printf("Function was called %d times\n", func_cnt);

  printf("c_record:\n%d : %d : %f \n",c_record.a, c_record.b, c_record.c);
  return 0;

}
