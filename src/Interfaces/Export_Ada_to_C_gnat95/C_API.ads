with Interfaces.C; use Interfaces.C;

package C_API is
  function My_Func (A : Int) return Int;
  pragma Export (C, My_Func, "my_func"); -- Export procedure

  Func_Cnt : Int := 0;
  pragma Export (C, Func_Cnt, "func_cnt"); -- Export Global variable

  type C_Struct is
     record
       A : Int;
       B : Long;
       D : Double;
     end record;
  Ada_Struct : C_Struct := (1,2,4.0);
  pragma Export (C, Ada_Struct, "c_record"); -- Export Global record

end C_API;
