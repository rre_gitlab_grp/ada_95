with Interfaces.C; use Interfaces.C;
with Ada.Text_IO; use Ada.Text_IO;
procedure Show_C_Func is
  function My_Func (A : Int) return Int
   with
    Import	=> True,
    Convention	=> C;
  V        : Int;
  Func_Cnt : Int
   with
    Import	=> True,
    Convention	=> C;
  -- We can access the func_cnt variable from test.c
begin
  V := My_Func (1);
  V := My_Func (2);
  V := My_Func (3);
  Put_Line ("Result is " & Int'Image (V));
  Put_Line ("Function was called " & Int'Image (Func_Cnt) & " times");
end Show_C_Func;
