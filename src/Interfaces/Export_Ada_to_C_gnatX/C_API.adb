package body C_API is
  function My_Func (A : Int) return Int is
  begin
    return A * 2;
  end My_Func;
end C_API;
