with Interfaces.C; use Interfaces.C;
package C_API is
  function My_Func (A : Int) return Int
   with
    Export	=> True,
    Convention	=> C,
    External_Name => "my_func";

  Func_Cnt : Int := 0
   with
    Export	=> True,
    Convention => C;

end C_API;
