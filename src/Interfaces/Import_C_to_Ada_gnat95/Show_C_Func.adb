with Interfaces.C; use Interfaces.C;
with Ada.Text_IO; use Ada.Text_IO;


procedure Show_C_Func is
  function My_Func (A : Int) return Int;
  pragma Import (Ada, My_Func, "my_func");
  --     We can access the My_Func function from test.c

  V_C : Interfaces.C.Int := 0;
  V : Integer := 0;

  Func_Cnt : Interfaces.C.Int;
  pragma Import (Ada, Func_Cnt, "func_cnt");
  --     We can access the func_cnt variable from test.c

begin
  V_C := My_Func (1);
  V := Integer (V_C);

  --    V := My_Func (2);
  --    V := My_Func (3);
  Put_Line ("Result is " & Integer'Image (V));
  Put_Line ("Function was called " & Int'Image (Func_Cnt) & " times");
end Show_C_Func;
