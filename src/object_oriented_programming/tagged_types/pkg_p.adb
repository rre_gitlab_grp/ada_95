with Ada.Text_IO; use Ada.Text_IO;
package body Pkg_P is
  Global_O1 : My_Class;  -- try gdb  Global_O1'tag
  -- Declare an object of type My_Class
  Global_O2 : Derived := (A => 12);  -- try gdb  Global_O2'tag
  -- Declare an object of type Derived
  Global_O3 : My_Class'Class := Global_O2;
  procedure Foo (Self : in out My_Class) is
  begin
    Put_Line ("In My_Class.Foo");
  end Foo;
--    procedure Foo (Self : in out Derived) is
--    begin
--      Put_Line ("In Derived.Foo, A = " & Integer'Image (Self.A));
--    end Foo;
end Pkg_P;
