with Pkg_P; use Pkg_P;
procedure View_Conversion is
  O1 : Derived := (A => 12);
  -- Declare an object of type Derived
  O2 : My_Class := My_Class (O1); -- view conversion
  O3 : My_Class'Class := O2;
begin
  Foo (O1);
  -- Non dispatching: Calls Derived.Foo
  Foo (O2);
  -- Non dispatching: Calls My_Class.Foo
  Foo (O3);
  -- Dispatching: Calls My_Class.Foo
end View_Conversion;
