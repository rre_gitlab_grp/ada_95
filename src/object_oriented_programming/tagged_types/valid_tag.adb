with Pkg_P; use Pkg_P;
with Ada.Text_IO; use Ada.Text_IO;
procedure Valid_Tag is
  O1 : My_Class;
  -- Declare an object of type My_Class
  O2 : Derived := (A => 12);
  -- Declare an object of type Derived
  O3 : My_Class'Class := O2;
  -- Now valid: My_Class'Class designates the classwide type for
  -- My_Class, which is the set of all types descending from My_Class
  -- (including My_Class).
begin
  Put_Line ("valid assignement ");
  Foo (O1);
  Foo (O2);

  --O3 := O1;

  --null;
end Valid_Tag;
