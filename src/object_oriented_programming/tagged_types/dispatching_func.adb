with Pkg_P; use Pkg_P;
procedure Dispatching_Func is
  O1 : My_Class;
  -- Declare an object of type My_Class
  O2 : Derived := (A => 12);
  -- Declare an object of type Derived
  O3 : My_Class'Class := O2;
  O4 : My_Class'Class := O1;
begin
  Foo (O1);
  -- Non dispatching: Calls My_Class.Foo
  Foo (O2);
  -- Non dispatching: Calls Derived.Foo
  Foo (O3);
  -- Dispatching: Calls Derived.Foo
  Foo (O4);
  -- Dispatching: Calls My_Class.Foo
end Dispatching_Func;
