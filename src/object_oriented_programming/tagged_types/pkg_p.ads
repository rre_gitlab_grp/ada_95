package Pkg_P is
  type My_Class is tagged null
   record;
  -- Just like a regular record, but with tagged qualifier
  -- Methods are outside of the type definition:
  procedure Foo (Self : in out My_Class);
  -- If you define a procedure taking a My_Class argument -- in the same package, it will be a method.
  -- Here's how you derive a tagged type:
  type Derived is new My_Class with
     record
       A : Integer;
       -- You can add fields in derived types.
     end record;
  --procedure Foo (Self : in out Derived); --overriding
  -- The "overriding" qualifier is optional, but if it is present,
  -- it must be valid.
end Pkg_P;
