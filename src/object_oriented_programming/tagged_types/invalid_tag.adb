with Pkg_P; use Pkg_P;
with Ada.Text_IO; use Ada.Text_IO;
procedure Valid_Tag is
  O1 : My_Class;
  -- Declaring an object of type My_Class
  O2 : Derived := (A => 12);
  -- Declaring an object of type Derived
  O3 : My_Class := O2;
  -- INVALID: Trying to assign a value of type derived to a variable of
  -- type My_Class.
begin
  Put_Line ("invalid assignement ");
  --null;
end Valid_Tag;
