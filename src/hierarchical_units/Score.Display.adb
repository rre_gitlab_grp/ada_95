with Ada.Text_IO;
use  Ada.Text_IO;
package body Score.Display is
  procedure Show_Scores is
  begin
    New_Line;
    Put_Line ("Score:  Home " &  Integer'Image (Home_Score) &
               "  Visitors " &   Integer'Image (Visitors_Score));
    New_Line;
    --Put_Line ("Body Score:  Home " &  Integer'Image (Home_Score_Body);

  end Show_Scores;
  ------------------------------------------
  procedure Show_Fouls is
    Name : Player_Name;
  begin

    Put_Line ("Home Team Fouls:");

    for Name in Home_Player'Range loop
      Put ("  ");
      Put (Player_Name'Image (Name));
      Put (Integer'Image (Foul_Status (Name)));
    end loop;

    New_Line;
    Put_Line ("Visiting Team Fouls:");

    for Name in Visiting_Player'Range loop
      Put ("  ");
      Put (Player_Name'Image (Name));
      Put (Integer'Image (Foul_Status (Name)));
    end loop;

    New_Line;

  end Show_Fouls;
  ------------------------------------------
end Score.Display;
------------------------------------------------------------
