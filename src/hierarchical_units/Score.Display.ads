----------------------- Score.Display ----------------------
--  This child package exports two procedures, which handle 
--  two kinds of display requests.  
------------------------------------------------------------
package Score.Display is
  procedure Show_Scores;
  procedure Show_Fouls;  
end Score.Display;