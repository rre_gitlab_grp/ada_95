------------------------------------------------------------
----------------------- Score.Events -----------------------
--  This child package exports two procedures, which handle
--  two kinds of events: shots made and fouls committed.
------------------------------------------------------------
package Score.Events is
  procedure Shot_Made     (By   : in Player_Name; Shot : in Shot_Type);
  procedure Foul_Committed (By   : in Player_Name);
end Score.Events;
------------------------------------------------------------
