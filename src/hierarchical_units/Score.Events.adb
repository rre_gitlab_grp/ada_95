with Ada.Text_IO;
use  Ada.Text_IO;
package body Score.Events is

  procedure Shot_Made (By   : in Player_Name;  Shot : in Shot_Type) is

    Points : Integer range 1 .. 3;

  begin

    if    Shot = Foul_Shot then Points := 1;
    elsif Shot = Two_Pointer then Points := 2;
    elsif Shot = Three_Pointer then Points := 3;
    end if;

    if By in Home_Player'Range then              -- membership test
      Home_Score := Home_Score + Points;
    elsif By in Visiting_Player'Range then       -- membership test
      Visitors_Score := Visitors_Score + Points;
    end if;

  end Shot_Made;
  -----------------------------------------------
  procedure Foul_Committed (By : in Player_Name) is
  begin
    Foul_Status (By) := Foul_Status (By) + 1;
  end Foul_Committed;
  -----------------------------------------------
end Score.Events;
------------------------------------------------------------
