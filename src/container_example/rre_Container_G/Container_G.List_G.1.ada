

with Rbc_Constants;

-- A container with the general behavior of a fully dynamic list supporting addition and removal of any element at any point in its lifecycle.
--
-- Elements can be removed from the container at any time, at any position.
--
-- The List container is intended for the storage of dynamic data sets such as Trains in the RBC application.
generic

  -- This operation will be called to reset the fields of Element objects prior to usage.
  with procedure Init (Self : access Element_T);

package Container_G.List_G is

  type List_T is new Container_T with private;

  function Get_Element_At_Pos (Self : access List_T; Pos  : in Index_Range_T) return Element_Ptr;

  function Get_Element_At_Pos (Self : in List_T; Pos  : in Index_Range_T) return Element_T;
  
  procedure Add_New (Self : in out List_T; Pos : out Pos_Range_T);

  procedure Add_New_At_Pos (Self : in out List_T; Pos : in out Index_Range_T);

  procedure Clear (Self : out List_T);
 
  procedure Remove_Element_At_Pos (Self : in out List_T; Pos : in Index_Range_T);
 
  procedure Next (It : in out Iterator_T; Self : in List_T);
 
  function New_Iterator (Self : in List_T) return Iterator_T;
 
  procedure Execute (Self : access List_T; This_Proc : in Proc_Access_T);
  
  procedure Execute (Self : access List_T; This_Proc : in Proc_Idx_Access_T);
  
  function Get (It : in Iterator_T; Self : access List_T) return Element_Ptr;

  function Get (It : in Iterator_T; Self : in List_T) return Element_T;

  function Getstatus (Self : in List_T; Pos  : in Index_Range_T) return Boolean;

private

  function Init_First_Free return Pos_Range_T;

  type Status_Array_T is array (Index_Range_T) of Boolean;

  type List_T is new Container_T with
    record
      T_Status       : Status_Array_T := (others => False);
      First_Free     : Pos_Range_T    := Init_First_Free;
      First_Relevant : Pos_Range_T    := Rbc_Constants.Null_Pos;
    end record;

end Container_G.List_G;
