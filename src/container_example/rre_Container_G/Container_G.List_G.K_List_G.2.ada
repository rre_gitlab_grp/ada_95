

with Rbc_Constants;

package body Container_G.List_G.K_List_G is


  procedure Add_New (Self : in out K_List_T; Id : in Id_T; Pos : out Pos_Range_T) is
  begin
    Add_New (Self, Pos);
    if Pos /= Rbc_Constants.Null_Pos then
      Set_Id (Self.T_Element (Pos), Id);
    end if;
  end Add_New;


  -- Pos should be a In paramater but is set to In Out because of a bug with the Aonix compiler
  procedure Add_New_At_Pos (Self : in out K_List_T; Id : in Id_T; Pos : in out Index_Range_T) is
  begin
    Add_New_At_Pos (Self, Pos);
    Set_Id (Self.T_Element (Pos), Id);
  end Add_New_At_Pos;


  function Get_Element_From_Key (Self : access K_List_T; Id : in Id_T) return Element_Ptr is
    Iterator : Iterator_T := New_Iterator (Self.all);
    Id_Found : Boolean    := False;
  begin
    while not (Id_Found or Done (Iterator, Self.all)) loop
      if Get_Id (Self.T_Element (Iterator)) = Id then
        Id_Found := True;
      else
        Next (Iterator, Self.all);
      end if;
    end loop;
    if Id_Found then
      return Get (Iterator, Self);
    else
      return null;
    end if;
  end Get_Element_From_Key;


  function Get_Pos_From_Key (Self : in K_List_T; Id : in Id_T) return Pos_Range_T is
    Iterator : Iterator_T := New_Iterator (Self);
    Id_Found : Boolean    := False;

  begin
    while not (Id_Found or Done (Iterator, Self)) loop
      if Get_Id (Self.T_Element (Iterator)) = Id then
        Id_Found := True;
      else
        Next (Iterator, Self);
      end if;
    end loop;
    if Id_Found then
      return Iterator;
    else
      return Rbc_Constants.Null_Pos;
    end if;
  end Get_Pos_From_Key;

end Container_G.List_G.K_List_G;
