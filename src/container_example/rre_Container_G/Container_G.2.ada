
with Types_Alstom; use Types_Alstom;
with Rbc_Constants;

package body Container_G is

  function Done (It : in Iterator_T; Self : in Container_T) return Boolean is
    pragma Unreferenced (Self);
    Report : Boolean;
  begin
    if It = Rbc_Constants.Null_Pos then
      Report := True;
    else
      Report := False;
    end if;
    return Report;
  end Done;


  procedure Execute (Self : access Container_T; This_Proc : in Proc_Access_T) is
  begin
    for I in Self.T_Element'First .. Self.Size loop
      This_Proc (Self.T_Element (I)'Access);
    end loop;
  end Execute;


  procedure Execute (Self : access Container_T; This_Proc : in Proc_Idx_Access_T) is
  begin
    for I in Self.T_Element'First .. Self.Size loop
      This_Proc (Self.T_Element (I)'Access, I);
    end loop;
  end Execute;


  function Selected_Subset   (Self : access Container_T'Class;    Ref  : in Reference_T) return Element_Set_T is
    Set     : Element_Set_T := (others => null);
    Current : Pos_Range_T   := 0;
  begin
    for I in Self.T_Element'Range loop
      if Getstatus (Self.all, I) and then Is_Selected (Elem => Self.T_Element (I)'Access, Ref  => Ref)
      then
        Current       := Current + 1;
        Set (Current) := Self.T_Element (I)'Access;
      end if;
    end loop;
    return Set;
  end Selected_Subset;


  function Selected_Element (Self : access Container_T'Class; Ref  : in Reference_T) return Element_Ptr is
  begin
    for I in Self.T_Element'Range loop
      if Getstatus (Self.all, I) and then Is_Selected (Elem => Self.T_Element (I)'Access, Ref  => Ref)
      then
        return Self.T_Element (I)'Access;
      end if;
    end loop;
    return null;
  end Selected_Element;


  function Getsize (Self : in Container_T) return Pos_Range_T is
  begin
    return Self.Size;
  end Getsize;


  function Get_Context (Self : in Container_T; It   : in Iterator_T) return Erreur.Context_T is
  begin
    return (1 => Long_T (Self.Size), 2 => Long_T (It), others => 0);
  end Get_Context;

end Container_G;
