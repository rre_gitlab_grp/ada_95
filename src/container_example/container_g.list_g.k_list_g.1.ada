
generic
  type Id_T is private;

  with function Get_Id
   (Element : in Element_T)
    return    Id_T;

  with procedure Set_Id
   (Element : in out Element_T;
    Id      : in Id_T);

package Container_G.List_G.K_List_G is

  type K_List_T is new List_T with private;


  procedure Add_New
   (Self : in out K_List_T;
    Id   : in     Id_T;
    Pos  :    out Pos_Range_T);

  procedure Add_New_At_Pos
   (Self : in out K_List_T;
    Id   : in     Id_T;
    Pos  : in out Index_Range_T);
  function Get_Element_From_Key
   (Self : access K_List_T;
    Id   : in     Id_T)
    return Element_Ptr;
  function Get_Pos_From_Key
   (Self : in K_List_T;
    Id   : in Id_T)
    return Pos_Range_T;

private

  type K_List_T is new List_T with null record;

end Container_G.List_G.K_List_G;
