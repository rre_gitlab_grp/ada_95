with Constants;
pragma Elaborate_All (Constants);

package Dyn_Constants is
  Nmax_Point : Integer := Constants.Read;
end Dyn_Constants;
