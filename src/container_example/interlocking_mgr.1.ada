with Interlocking_Mgr_Points_Container;
with Interlocking_Mgr_Points_Container.List;
with Interlocking_Mgr_Points_Container.List.K_List;
with Rm_Data_Types;
with Dyn_Constants;
with Rbc_Data_Types;

package Interlocking_Mgr is
  package Interlocking_Mgr_Points_K_List renames Interlocking_Mgr_Points_Container.List.K_List;
  type Interlocking_Mgr_T is record
    Points_Lst            : aliased Interlocking_Mgr_Points_K_List.K_List_T;
  end record;
  type Array_T is array (1 .. 4) of Rbc_Data_Types.Identifier_T;

  subtype Points_Card_T is Rm_Data_Types.Index_Point_T range 1 .. Rm_Data_Types.Index_Point_T (Dyn_Constants.Nmax_Point);

  function Get_Points_At_Pos
   (Pos  : in Points_Card_T)
    return Interlocking_Mgr_Points_Container.Element_Ptr;

  procedure Init_Points;
  procedure Init_Point_Novram;
  procedure Code_Point_Novram;
  procedure Code_Point_Novram_New;
  function Get_Point_Pos_From_Key
   (Id   : in Rbc_Data_Types.Identifier_T)
    return Rm_Data_Types.Index_Point_T;

  procedure Remove_Point_At_Pos (Pos : in Points_Card_T);

end Interlocking_Mgr;
