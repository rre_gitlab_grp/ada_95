

package Types_Alstom is
  pragma Elaborate_Body (Types_Alstom);  Bits : constant := 1;  Zero : constant := 0;
  One  : constant := 1;
  subtype Boolean_T is Boolean;
  subtype Char_T is Character;
  subtype Ascii_T is Char_T;
  type Switch_T is (Disabled, Enabled);
  type Bit_T is range 0 .. 1;
  for Bit_T'Size use 1 * Bits;
  type Byte_T is range -(2**7) .. (2**7) - 1;
  for Byte_T'Size use 8 * Bits;
  subtype Natural_Byte_T is Byte_T range 0 .. Byte_T'Last;
  subtype Positive_Byte_T is Byte_T range 1 .. Byte_T'Last;
  type Unsigned_Byte_T is range 0 .. (2**8) - 1;
  for Unsigned_Byte_T'Size use 8 * Bits;
  type Unchecked_Byte_T is mod (2**8);
  for Unchecked_Byte_T'Size use 8 * Bits;
  pragma Suppress (Overflow_Check, On => Unchecked_Byte_T);
  pragma Suppress (Range_Check, On => Unchecked_Byte_T);
  subtype Octet_T is Unsigned_Byte_T;
  type Word_T is range -(2**15) .. (2**15) - 1;
  for Word_T'Size use 16 * Bits;  subtype Natural_Word_T is Word_T range 0 .. Word_T'Last;
  subtype Positive_Word_T is Word_T range 1 .. Word_T'Last;
  type Unsigned_Word_T is range 0 .. (2**16) - 1;
  for Unsigned_Word_T'Size use 16 * Bits;
  type New_Index_T is new Unsigned_Word_T;
  subtype Array_Index_T is New_Index_T;
  subtype String_Size_T is New_Index_T;
  subtype Size_In_Bytes_T is New_Index_T;  subtype String_Idx_T is String_Size_T range 1 .. String_Size_T'Last;
  type Unchecked_Word_T is mod (2**16);
  for Unchecked_Word_T'Size use 16 * Bits;
  pragma Suppress (Overflow_Check, On => Unchecked_Word_T);
  pragma Suppress (Range_Check, On => Unchecked_Word_T);
  subtype Mot_T is Unsigned_Word_T;
  pragma Obsolescent (Mot_T, Message => "UNSIGNED_WORD_T should be used instead of MOT_T");
  type Signed_24_T is range -2**23 .. 2**23 - 1;
  for Signed_24_T'Size use 24;
  type Unsigned_24_T is range 0 .. 2**24 - 1;
  for Unsigned_24_T'Size use 24;
  type Long_T is range -(2**31) .. (2**31) - 1;
  for Long_T'Size use 32 * Bits;  subtype Natural_Long_T is Long_T range 0 .. Long_T'Last;
  subtype Positive_Long_T is Long_T range 1 .. Long_T'Last;
  subtype Entier_Long_T is Long_T; 
  pragma Obsolescent (Entier_Long_T, Message => "LONG_T should be used instead of ENTIER_LONG_T");
  subtype Naturel_Long_T is Natural_Long_T; 
  pragma Obsolescent (Naturel_Long_T, Message => "NATURAL_LONG_T should be used instead of NATUREL_LONG_T");
  subtype Positif_Long_T is Positive_Long_T; 
  pragma Obsolescent (Positif_Long_T, Message => "POSITIVE_LONG_T should be used instead of POSITIF_LONG_T");
  subtype Signed_Double_Word_T is Long_T;
  type Unsigned_Long_T is range 0 .. (2**31) - 1;
  for Unsigned_Long_T'Size use 32 * Bits;  subtype Unsigned_Double_Word_T is Unsigned_Long_T;
  type Unchecked_Long_T is mod (2**32);
  for Unchecked_Long_T'Size use 32 * Bits;
  pragma Suppress (Overflow_Check, On => Unchecked_Long_T);
  pragma Suppress (Range_Check, On => Unchecked_Long_T);  subtype Modulo_Double_Word_T is Unchecked_Long_T;

  type Long_64_T is private;
  function Image
   (From : in Long_64_T)
    return String;
  pragma Obsolescent (Image, Message => "Image for Long_64_T does not exist on target !");
  subtype Signed_Quad_Word_T is Long_64_T;

  subtype Constrained_Float_T is Float range Float'First .. Float'Last;

  type Real_T is record
    Value : Constrained_Float_T;
  end record;

  for Real_T'Size use 32;
  for Real_T'Alignment use 4;

  type Bit_Array_T is array (Array_Index_T range <>) of Bit_T;
  type Bits_T is array (Long_T range <>) of Bit_T;
  pragma Pack (Bits_T);
  type Booleans_T is array (Long_T range <>) of Boolean_T;
  pragma Pack (Booleans_T);

  type Byte_Array_T is array (Array_Index_T range <>) of Unsigned_Byte_T;
  type Bytes_T is array (Long_T range <>) of Unsigned_Byte_T;

  type Octets_T is array (Integer range <>) of Octet_T;
  pragma Obsolescent (Octets_T, Message => "BYTES_T should be used instead of OCTETS_T");

  type Word_Array_T is array (Array_Index_T range <>) of Unsigned_Word_T;
  type Words_T is array (Long_T range <>) of Word_T;

  type Mots_T is array (Integer range <>) of Mot_T;
  pragma Obsolescent (Mots_T, Message => "WORDS_T should be used instead of MOTS_T");

  type Long_Array_T is array (Array_Index_T range <>) of Unsigned_Long_T;  type Longs_T is array (Long_T range <>) of Long_T;
  type Byte_In_Bits_T is array (1 .. 8) of Bit_T;
  pragma Pack (Byte_In_Bits_T);
  for Byte_In_Bits_T'Size use 8 * Bits;
  subtype Octet_En_Bits_T is Byte_In_Bits_T;
  pragma Obsolescent (Octet_En_Bits_T, Message => "BYTE_IN_BITS_T should be used instead of OCTET_EN_BITS_T");
  subtype Byte_String_T is Byte_Array_T;  type Compteur_8_T is mod (2**8);
  pragma Obsolescent (Compteur_8_T, Message => "UNCHECKED_BYTE_T should be used instead of COMPTEUR_8_T");
  for Compteur_8_T'Size use 8 * Bits;
  type Compteur_16_T is mod (2**16);
  pragma Obsolescent (Compteur_16_T, Message => "UNCHECKED_WORD_T should be used instead of COMPTEUR_16_T");
  for Compteur_16_T'Size use 16 * Bits;
  type Valid_Unchecked_Long_T is record
    Is_Valid : Boolean_T        := False;
    Value    : Unchecked_Long_T := 0;
  end record;
  type Shift_T is range 1 .. 30;
  type Byte_Data_T (Max_Size : Array_Index_T) is record
    Size : Array_Index_T                := 0;
    Data : Byte_Array_T (1 .. Max_Size) := (others => 0);
  end record;
  type Bit_Data_T (Max_Size : Array_Index_T) is record
    Size : Array_Index_T               := 0;
    Data : Bit_Array_T (1 .. Max_Size) := (others => 0);
  end record;
  type Gofast_Error_Code is new Unchecked_Long_T;
  Long_64_T_First : constant Long_64_T;
  Long_64_T_Zero : constant Long_64_T;
  Long_64_T_Last : constant Long_64_T;
  Long_64_T_One : constant Long_64_T;
  Long_64_T_Ten : constant Long_64_T;
  Long_64_T_Minus_One : constant Long_64_T;
  Nul_Real_C : constant Real_T := (Value => 0.0);
  Null_Byte_Array_C : constant Byte_Array_T (1 .. 0) := (others => 0);

  function "not"
   (Op : in Unsigned_Byte_T)
    return Unsigned_Byte_T;
  function "or"
   (G, D : in Unsigned_Byte_T)
    return Unsigned_Byte_T;
  function "and"
   (G, D : in Unsigned_Byte_T)
    return Unsigned_Byte_T;
  function "xor"
   (G, D : in Unsigned_Byte_T)
    return Unsigned_Byte_T;
  pragma Inline ("not", "or", "and", "xor");

  function "not"
   (Op : in Unsigned_Word_T)
    return Unsigned_Word_T;
  function "or"
   (G, D : in Unsigned_Word_T)
    return Unsigned_Word_T;
  function "and"
   (G, D : in Unsigned_Word_T)
    return Unsigned_Word_T;
  function "xor"
   (G, D : in Unsigned_Word_T)
    return Unsigned_Word_T;

  pragma Inline ("not", "or", "and", "xor");

  function "not"
   (Op : in Unsigned_Long_T)
    return Unsigned_Long_T;
  function "or"
   (G, D : in Unsigned_Long_T)
    return Unsigned_Long_T;
  function "and"
   (G, D : in Unsigned_Long_T)
    return Unsigned_Long_T;
  function "xor"
   (G, D : in Unsigned_Long_T)
    return Unsigned_Long_T;
  pragma Inline ("not", "or", "and", "xor");
  function ">="
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Boolean_T;
  function ">"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Boolean_T;
  function "<="
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Boolean_T;
  function "<"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Boolean_T;
  function ">="
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T;
  function ">"
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T;
  function "<="
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T;
  function "<"
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T;
  function "="
   (Left  : Long_64_T;
    Right : Long_T)
    return Boolean_T;
  function ">="
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T;
  function ">"
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T;
  function "<="
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T;
  function "<"
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T;
  function "="
   (Left  : Long_T;
    Right : Long_64_T)
    return Boolean_T;
  function "="
   (Left, Right : in Real_T)
    return Boolean_T;
  function ">"
   (Left, Right : in Real_T)
    return Boolean_T;
  function "<"
   (Left, Right : in Real_T)
    return Boolean_T;
  function ">="
   (Left, Right : in Real_T)
    return Boolean_T;
  function "<="
   (Left, Right : in Real_T)
    return Boolean_T;
  pragma Inline ("=", ">", "<", ">=", "<=");
  function "+"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Long_64_T;
  function "-"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Long_64_T;

  function "+"
   (Left  : Long_64_T;
    Right : Long_T)
    return Long_64_T;
  function "-"
   (Left  : Long_64_T;
    Right : Long_T)
    return Long_64_T;

  function "+"
   (Left  : Long_T;
    Right : Long_64_T)
    return Long_64_T;
  function "-"
   (Left  : Long_T;
    Right : Long_64_T)
    return Long_64_T;

  function "+"
   (Left, Right : in Real_T)
    return Real_T;
  function "-"
   (Left, Right : in Real_T)
    return Real_T;

  pragma Inline ("+", "-");
  function "-"
   (From : Long_64_T)
    return Long_64_T;
  function "abs"
   (From : Long_64_T)
    return Long_64_T;
  function "+"
   (From : in Real_T)
    return Real_T;
  function "-"
   (From : in Real_T)
    return Real_T;
  function "abs"
   (From : in Real_T)
    return Real_T;
  pragma Inline ("+", "-", "abs");

  function Increment
   (Le_Compteur : in Compteur_8_T)
    return Compteur_8_T;
  function Increment
   (Le_Compteur : in Compteur_16_T)
    return Compteur_16_T;
  function "*"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Long_64_T;  function Mult_32_To_64
   (Left  : Long_T;
    Right : Long_T)
    return Long_64_T;  function Mult_32_To_64
   (Left  : Unchecked_Long_T;
    Right : Unchecked_Long_T)
    return Long_64_T;
  function "*"
   (Left  : Long_64_T;
    Right : Long_T)
    return Long_64_T;

  function "/"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Long_64_T;
  function "/"
   (Left  : Long_64_T;
    Right : Long_T)
    return Long_64_T;
  function "mod"
   (Left  : Long_64_T;
    Right : Long_T)
    return Long_T;
  function "mod"
   (Left  : Long_64_T;
    Right : Long_64_T)
    return Long_64_T;

  function Sqrt
   (From : Long_64_T)
    return Long_64_T;
  function "*"
   (Left, Right : in Real_T)
    return Real_T;
  function "/"
   (Left, Right : in Real_T)
    return Real_T;
  pragma Inline ("*", "/");

  function "**"
   (Left  : in Real_T;
    Right :    Long_T)
    return Real_T;
  pragma Inline ("**");

  function Sqrt
   (From : in Real_T)
    return Real_T;
  pragma Inline (Sqrt);
  function Shift_Left
   (On     : in Unchecked_Byte_T;
    Shifts : in Shift_T)
    return Unchecked_Byte_T;
  pragma Inline (Shift_Left);
  function Shift_Left
   (On     : in Unchecked_Word_T;
    Shifts : in Shift_T)
    return Unchecked_Word_T;
  pragma Inline (Shift_Left);
  function Shift_Left
   (On     : in Unchecked_Long_T;
    Shifts : in Shift_T)
    return Unchecked_Long_T;
  pragma Inline (Shift_Left);
  function Shift_Right
   (On     : in Unchecked_Byte_T;
    Shifts : in Shift_T)
    return Unchecked_Byte_T;
  pragma Inline (Shift_Right);
  function Shift_Right
   (On     : in Unchecked_Word_T;
    Shifts : in Shift_T)
    return Unchecked_Word_T;
  pragma Inline (Shift_Right);
  function Shift_Right
   (On     : in Unchecked_Long_T;
    Shifts : in Shift_T)
    return Unchecked_Long_T;
  pragma Inline (Shift_Right);
  function Shift_Left_Byte
   (On     : in Unchecked_Byte_T;
    Shifts : in Shift_T)
    return Unchecked_Byte_T renames Shift_Left;
  function Shift_Left_Word
   (On     : in Unchecked_Word_T;
    Shifts : in Shift_T)
    return Unchecked_Word_T renames Shift_Left;
  function Shift_Left_Long
   (On     : in Unchecked_Long_T;
    Shifts : in Shift_T)
    return Unchecked_Long_T renames Shift_Left;
  function Shift_Right_Byte
   (On     : in Unchecked_Byte_T;
    Shifts : in Shift_T)
    return Unchecked_Byte_T renames Shift_Right;
  function Shift_Right_Word
   (On     : in Unchecked_Word_T;
    Shifts : in Shift_T)
    return Unchecked_Word_T renames Shift_Right;
  function Shift_Right_Long
   (On     : in Unchecked_Long_T;
    Shifts : in Shift_T)
    return Unchecked_Long_T renames Shift_Right;
  function Byte
   (Byte_In_Bits : in Byte_In_Bits_T)
    return Unsigned_Byte_T;
  pragma Inline (Byte);
  function Byte_In_Bits
   (Byte : in Unsigned_Byte_T)
    return Byte_In_Bits_T;
  pragma Inline (Byte_In_Bits);
  function Octet
   (Octet_En_Bits : in Octet_En_Bits_T)
    return Octet_T renames Byte;
  pragma Obsolescent (Octet, Message => "BYTE should be used instead of OCTET");
  function Octet_En_Bits
   (Octet : in Octet_T)
    return Octet_En_Bits_T renames Byte_In_Bits;
  pragma Obsolescent (Octet_En_Bits, Message => "BYTE_IN_BITS should be used instead of OCTET_EN_BITS");
  function Msb
   (W : in Unsigned_Word_T)
    return Unsigned_Byte_T;
  pragma Inline (Msb);  function Lsb
   (W : in Unsigned_Word_T)
    return Unsigned_Byte_T;
  pragma Inline (Lsb);  function Msb
   (W : in Unchecked_Word_T)
    return Unchecked_Byte_T;
  pragma Inline (Msb);  function Lsb
   (W : in Unchecked_Word_T)
    return Unchecked_Byte_T;
  pragma Inline (Lsb);
  function Octet_Bas
   (Du_Mot : in Unsigned_Word_T)
    return Unsigned_Byte_T renames Lsb;
  pragma Obsolescent (Octet_Bas, Message => "LSB should be used instead of OCTET_BAS");
  function Octet_Haut
   (Du_Mot : in Unsigned_Word_T)
    return Unsigned_Byte_T renames Msb;
  pragma Obsolescent (Octet_Haut, Message => "MSB should be used instead of OCTET_HAUT");
  function Lsw
   (W : in Unsigned_Long_T)
    return Unsigned_Word_T;  function Msw
   (W : in Unsigned_Long_T)
    return Unsigned_Word_T;  function Lsw
   (W : in Unchecked_Long_T)
    return Unchecked_Word_T;
  pragma Inline (Lsw);  function Msw
   (W : in Unchecked_Long_T)
    return Unchecked_Word_T;
  pragma Inline (Msw);
  function Lsw
   (From : in Long_64_T)
    return Unchecked_Long_T;  function Msw
   (From : in Long_64_T)
    return Long_T;
  procedure Split
   (Item : in     Long_64_T;
    High :    out Long_T;
    Low  :    out Unchecked_Long_T);

  
  function To_Long
   (From : in Long_64_T)
    return Long_T;
  function Checked_To_Long
   (From : in Long_64_T)
    return Long_T;  function To_Unsigned_Long
   (From : in Long_64_T)
    return Unsigned_Long_T;
  function Managed_To_Unsigned_Long
   (From : in Long_64_T)
    return Unsigned_Long_T;  function To_Long_T
   (From : in Long_64_T)
    return Long_T renames To_Long;
  pragma Obsolescent (To_Long_T, Message => "TO_LONG should be used instead of TO_LONG_T");
  function Checked_To_Long_T
   (From : in Long_64_T)
    return Long_T renames Checked_To_Long;
  pragma Obsolescent (Checked_To_Long_T, Message => "CHECKED_TO_LONG should be used instead of CHECKED_TO_LONG_T");
  function To_Unsigned_Long_T
   (From : in Long_64_T)
    return Unsigned_Long_T renames To_Unsigned_Long;
  pragma Obsolescent (To_Unsigned_Long_T, Message => "TO_UNSIGNED_LONG should be used instead of TO_UNSIGNED_LONG_T");
  function Managed_To_Unsigned_Long_T
   (From : in Long_64_T)
    return Unsigned_Long_T renames Managed_To_Unsigned_Long;
  pragma Obsolescent (Managed_To_Unsigned_Long_T, Message => "MANAGED_TO_UNSIGNED_LONG should be used instead of MANAGED_TO_UNSIGNED_LONG_T");
  function Make_Word
   (Msb : in Unsigned_Byte_T;
    Lsb : in Unsigned_Byte_T)
    return Unsigned_Word_T;  function Make_Word
   (Msb : in Unchecked_Byte_T;
    Lsb : in Unchecked_Byte_T)
    return Unchecked_Word_T;
  pragma Inline (Make_Word);
  function Mot
   (Octet_Haut : in Unsigned_Byte_T;
    Octet_Bas  : in Unsigned_Byte_T)
    return Unsigned_Word_T renames Make_Word;
  pragma Obsolescent (Mot, Message => "MAKE_WORD should be used instead of MOT");
  function Unsigned_Word
   (Msb : in Unsigned_Byte_T;
    Lsb : in Unsigned_Byte_T)
    return Unsigned_Word_T renames Make_Word;
  pragma Obsolescent (Unsigned_Word, Message => "MAKE_WORD should be used instead of UNSIGNED_WORD");  function Make_Long
   (Msw : in Unchecked_Word_T;
    Lsw : in Unchecked_Word_T)
    return Unchecked_Long_T;
  pragma Inline (Make_Long);
  function To_Long_64
   (From : in Long_T)
    return Long_64_T;  function To_Long_64
   (From : in Unsigned_Long_T)
    return Long_64_T;  function Make_Long_64
   (Msw : in Long_T;
    Lsw : in Unchecked_Long_T)
    return Long_64_T;
  function To_Long_64_T
   (From : in Long_T)
    return Long_64_T renames To_Long_64;
  pragma Obsolescent (To_Long_64_T, Message => "TO_LONG_64 should be used instead of TO_LONG_64_T");
  function To_Long_64_T
   (From : in Unsigned_Long_T)
    return Long_64_T renames To_Long_64;
  pragma Obsolescent (To_Long_64_T, Message => "TO_LONG_64 should be used instead of TO_LONG_64_T");
  function Make_Long_64_T
   (Msw : in Long_T;
    Lsw : in Unchecked_Long_T)
    return Long_64_T renames Make_Long_64;
  pragma Obsolescent (Make_Long_64_T, Message => "MAKE_LONG_64 should be used instead of MAKE_LONG_64_T");
  function Value_Of
   (High : Long_T;
    Low  : Unchecked_Long_T)
    return Long_64_T renames Make_Long_64;
  pragma Obsolescent (Value_Of, Message => "MAKE_LONG_64 should be used instead of VALUE_OF");
  generic
    type T is range <>;
  function Real_Of_Int
   (From : in T)
    return Real_T;  generic
    type T is delta <>;
  function Real_Of_Fixed
   (From : in T)
    return Real_T;
  function To_Real
   (From : in Long_64_T)
    return Real_T;  function To_Real_T
   (From : in Long_64_T)
    return Real_T renames To_Real;  function To_Long_64
   (From : in Real_T)
    return Long_64_T;

  generic
    type T is range <>;
  function Int_Of_Real
   (From : in Real_T)
    return T;

  generic
    type T is delta <>;
  function Fixed_Of_Real
   (From : in Real_T)
    return T;
  Error_Handling_Enabled : Boolean_T := True;
  Nan_Positive : constant Gofast_Error_Code := 16#7FC0_0000#;
  Nan_Negative : constant Gofast_Error_Code := 16#FFC0_0000#;

  Infinity_Positive : constant Gofast_Error_Code := 16#7F80_0000#;
  Infinity_Negative : constant Gofast_Error_Code := 16#FF80_0000#;

  Overflow_Positive : constant Gofast_Error_Code := 16#7FFF_FFFF#;
  Overflow_Negative : constant Gofast_Error_Code := 16#FFFF_FFFF#;

  Gofast_Return_Value : Gofast_Error_Code;
  Argument_Error : exception;
  procedure Enable_Real_Error (Enable : Boolean_T);
  function Is_Enable_Real_Error return Boolean_T;
  package Counter is

    type Operation_T is
     (Float_Add,
      Float_Sub,
      Float_Mul,
      Float_Div,
      Float_Sqr,
      Float_Exp,
      Float_Ln,
      Float_Abs_1,
      Float_Add_1,
      Float_Sub_1,
      Float_Gt,
      Float_Lt,
      Float_Gte,
      Float_Lte,
      Float_Eq,
      Float_Conv_From_Int,
      Float_Conv_To_Int,
      Float_Conv_From_Fix,
      Float_Conv_To_Fix,
      Float_Total_Time,

      Int64_Add,
      Int64_Sub,
      Int64_Mul,
      Int64_Div,
      Int64_Mod,
      Int64_Abs_1,
      Int64_Add_1,
      Int64_Sub_1,
      Int64_Gt,
      Int64_Lt,
      Int64_Gte,
      Int64_Lte,
      Int64_Conv_To_Int,
      Int64_Conv_From_Int,
      Int64_Conv_To_Real,
      Int64_Total_Time,

      Logic_Not,
      Logic_Or,
      Logic_And,
      Logic_Xor,
      Logic_Total_Time,

      Shift_Left,
      Shift_Right,
      Shift_Total_Time,

      Total_Time);

    subtype Float_Operation_T is Operation_T range Float_Add .. Float_Total_Time;
    subtype Int64_Operation_T is Operation_T range Int64_Add .. Int64_Total_Time;
    subtype Logic_Operation_T is Operation_T range Logic_Not .. Logic_Total_Time;
    subtype Shift_Operation_T is Operation_T range Shift_Left .. Shift_Total_Time;

    type Operation_Selector_T is array (Operation_T) of Boolean;
    All_Operation_C   : constant Operation_Selector_T := (others => True);
    Float_Operation_C : constant Operation_Selector_T := (Float_Operation_T => True, others => False);
    Int64_Operation_C : constant Operation_Selector_T := (Int64_Operation_T => True, others => False);
    Logic_Operation_C : constant Operation_Selector_T := (Logic_Operation_T => True, others => False);
    Shift_Operation_C : constant Operation_Selector_T := (Shift_Operation_T => True, others => False);

    type Cont_Kind_T is (Min, Max, Sum, Last_Cycle, Mean, All_Kind);

    type Logging_Access is access procedure (Msg : in String);

  
    procedure Init (Logging_Proc : in Logging_Access);

  
    procedure Display_Operation_Count
     (For_Operation : in Operation_Selector_T := All_Operation_C;
      Cont_Kind     : in Cont_Kind_T          := All_Kind);

    
    
    procedure Reset_Operation_Count;

  
  
    procedure Increment_Operation_Cnt (Op : in Operation_T);
    pragma Inline (Increment_Operation_Cnt);

  end Counter;
private

  type Long_64_T is new Long_Long_Integer;
  type Target_Long_64_T is record
    Msw : Long_T           := 0;
    Lsw : Unchecked_Long_T := 0;
  end record;
  for Target_Long_64_T'Alignment use 4;
  for Target_Long_64_T use record
    Msw at 4 range 0 .. 31;
    Lsw at 0 range 0 .. 31;
  end record;

  Long_64_T_First : constant Long_64_T := Long_64_T'First;

  Long_64_T_Zero : constant Long_64_T := 0;

  Long_64_T_Last : constant Long_64_T := Long_64_T'Last;

  Long_64_T_One : constant Long_64_T := 1;

  Long_64_T_Ten : constant Long_64_T := 10;

  Long_64_T_Minus_One : constant Long_64_T := Long_64_T (-1);

end Types_Alstom;
