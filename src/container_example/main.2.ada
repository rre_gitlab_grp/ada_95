with ADA.TEXT_IO;
with Ada.Integer_Text_IO;
with Interlocking_Mgr;
with Interlocking_Mgr_Points_Container;
with Interlocking_Mgr_Points_Container.List;
with Interlocking_Mgr_Points_Container.List.K_List;
with Rm_Data_Types;
with Dyn_Constants;
with Rbc_Data_Types;
with Point;
with Rbc_Constants;

procedure Main is

  package Interlocking_Mgr_Points_K_List renames Interlocking_Mgr_Points_Container.List.K_List;

  This_Point  :  Interlocking_Mgr_Points_Container.Element_Ptr;
  I           : Integer := 0;
  Id          : Rbc_Data_Types.Identifier_T := "Points_000000003";
  Id2         : Rbc_Data_Types.Identifier_T := "Points_000000007";
  Point_Index : Rm_Data_Types.Index_Point_T;

  use type Rm_Data_Types.Index_Point_Base;
  use type Rm_Data_Types.Point_State_T;

  procedure Check_And_Get_Point_By_Key (Point_Id : Rbc_Data_Types.Identifier_T) is
    use type Rm_Data_Types.Index_Point_Base;
    My_Point_Object_Ptr : Point.Point_Acc_T;
  begin
    Ada.Text_IO.Put_Line (" Check_And_Get_Point() In for " & Point_Id );

    Point_Index := Interlocking_Mgr.Get_Point_Pos_From_Key (Id => Point_Id);
    if (Point_Index = Rm_Data_Types.Index_Point_Base (Rbc_Constants.Null_Pos)) then
      Ada.Text_IO.Put_Line ("\n Point_Id :" & Point_Id  & " doesn't exist");
    else
      Ada.Text_IO.Put_Line ("\n Point_Id :" & Point_Id & " exists");
      My_Point_Object_Ptr := Interlocking_Mgr.Get_Points_At_Pos (Pos => Point_Index);
      if (Point.Get_Point_State (This => My_Point_Object_Ptr.all) = Rm_Data_Types.Blocked) then
        Ada.Text_IO.Put_Line ("\n Point_Id :" & Point_Id & " is blocked");
      else
        Ada.Text_IO.Put_Line ("\n Point_Id :" & Point_Id & " is not blocked");
      end if;
    end if;

  end Check_And_Get_Point_By_Key;
begin

  Ada.TEXT_IO.Put_Line ("\n Main...");
  Interlocking_Mgr.Init_Points;

  for I in Interlocking_Mgr.Points_Card_T'Range
  loop
    This_Point := Interlocking_Mgr.Get_Points_At_Pos (Pos => I);
    Point.Init_Object (This => This_Point);
    Ada.Text_IO.Put_Line ("Get_Points_At_Pos() :" & Interlocking_Mgr.Points_Card_T'Image (I));
  end loop;

  Interlocking_Mgr.Init_Point_Novram;

  Interlocking_Mgr.Code_Point_Novram;

  Check_And_Get_Point_By_Key (Point_Id => Id);
  Check_And_Get_Point_By_Key (Point_Id => Id2);

  Ada.Text_IO.Put_Line ("Before Removal testing() :");

  Point_Index := Interlocking_Mgr.Get_Point_Pos_From_Key (Id => Id);
  Interlocking_Mgr.Remove_Point_At_Pos (Pos => Point_Index);

  for I in Interlocking_Mgr.Points_Card_T'Range
  loop
    This_Point := Interlocking_Mgr.Get_Points_At_Pos (Pos => I);
    Point.Init_Object (This => This_Point);
    Ada.Text_IO.Put_Line ("Get_Points_At_Pos() :" & Interlocking_Mgr.Points_Card_T'Image (I) & ",Key:" & Point.Get_Id (This => This_Point.all));
  end loop;

end Main;
