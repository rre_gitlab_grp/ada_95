with Ada.Integer_Text_IO;
with Ada.Text_IO;
package body Constants is
  function Read return Integer is
    Max_Value : Integer := 0;
  begin
    Ada.Text_IO.Put_Line("\n Enter the max_value for KList at elaboration:");
    Ada.Integer_Text_IO.Get (Max_Value);
    return Max_Value;
  end Read;
end Constants;
