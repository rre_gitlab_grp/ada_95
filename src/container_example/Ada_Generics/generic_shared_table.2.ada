-------------------------------------------------------------------------------
--                     COPYRIGHT : ALSTOM BELGIUM TRANSPORT
-------------------------------------------------------------------------------
-- HISTORY:
--------------------------------------------------------------------------------
--  DATE        NAME        DETAILS
--------------------------------------------------------------------------------
-- Older version where managed in Apex at St Ouen ------------------------------
--------------------------------------------------------------------------------
-- 08/11/2012 D.PAUWELS          ALPHA00320372 : Improve RBC code portability on PC
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Parent package.
-- ===============
--
-- This package includes all procedures that are public and shared with all users of the shared buffer.
--
-- See specification for documentation on functions and procedures.
--
-------------------------------------------------------------------------------

package body Generic_Shared_Table is

  -- Local Types_Alstom declaration.
  -- ------------------------

  -- Initialising procedure.
  -- -----------------------
  procedure Re_Init_Table is
  begin
    Max_Index_In_Use := 0;
  end Re_Init_Table;

  -- Getting current number of elements procedure.
  -- ---------------------------------------------
  function Current_Nb_Of_Elements return Element_Index is
  begin
    return Max_Index_In_Use;
  end Current_Nb_Of_Elements;

  -- Getting an element of the shared table.
  -- ---------------------------------------
  function Read_Only_Element
   (Index : Element_Index)
    return  Element_Read_Access is
  begin

    -- Check if received index is in acceptable range.
    -- -----------------------------------------------
    if ((Index <= 0) or (Index > Max_Index_In_Use)) then
      return null;
    end if;

    -- Give access on requested componant of the table.
    -- ------------------------------------------------
    return Table (Index)'Access;

  end Read_Only_Element;

  -- Writing an element into the shared table.
  -- -----------------------------------------
  function Next_Writable_Element return Element_Full_Access is
  begin

    -- Check if table is not full.
    -- ---------------------------
    if (Max_Index_In_Use >= Table'Last) then
      return null;
    end if;

    -- Set index on next componant of the table.
    -- -----------------------------------------
    Max_Index_In_Use := Max_Index_In_Use + 1;

    -- Give access to this new componant.
    -- ----------------------------------
    return Table (Max_Index_In_Use)'Access;

  end Next_Writable_Element;

  -- Getting an element of the shared table.
  -- ---------------------------------------
  function Writable_Element
   (Index : Element_Index)
    return  Element_Full_Access is
  begin

    -- Check if received index is in acceptable range.
    -- -----------------------------------------------
    if ((Index <= 0) or (Index > Max_Index_In_Use)) then
      return null;
    end if;

    -- Give access on requested componant of the table.
    -- ------------------------------------------------
    return Table (Index)'Access;

  end Writable_Element;

end Generic_Shared_Table;
