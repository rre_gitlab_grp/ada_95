-------------------------------------------------------------------------------
--                     COPYRIGHT : ALSTOM BELGIUM TRANSPORT
-------------------------------------------------------------------------------
-- HISTORY:
-- VER   DATE        NAME        DETAILS
-------------------------------------------------------------------------------
-- Older version where managed in Apex at St Ouen -----------------------------
-------------------------------------------------------------------------------

with Appli_Basic_Interface_Data_Types;
with Adjacent_Rbc_In_Message_Bin;
with Generic_Shared_Table;

pragma Elaborate_All (Generic_Shared_Table);

-- Interfaces with adjacents RBC's.
-- --------------------------------
-- Incoming messages from adjacent RBC's.
package Adjacent_Rbc_In_Shared is new Generic_Shared_Table (
  Element        => Adjacent_Rbc_In_Message_Bin.Adjacent_Rbc_In_Message_Bin_T,
  N_Max_Elements => Appli_Basic_Interface_Data_Types.Nmax_Rbc_In_Messages_C);
