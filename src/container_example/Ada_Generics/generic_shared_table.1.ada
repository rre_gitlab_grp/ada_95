-------------------------------------------------------------------------------
--                     COPYRIGHT : ALSTOM BELGIUM TRANSPORT
-------------------------------------------------------------------------------
-- HISTORY:
--------------------------------------------------------------------------------
--  DATE        NAME        DETAILS
--------------------------------------------------------------------------------
-- Older version where managed in Apex at St Ouen ------------------------------
--------------------------------------------------------------------------------
-- 08/11/2012 D.PAUWELS          ALPHA00320372 : Improve RBC code portability on PC
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- Parent package.
-- ===============
--
-- This package includes all definitions that are public and shared with all users of the shared buffer.
--
-- (With AONIX collaboration.)
-------------------------------------------------------------------------------

with Types_Alstom;

generic

  type Element is private;
  N_Max_Elements : in Types_Alstom.Natural_Long_T;

  -- contains a shared table and all associated services for initialisation, reading and writing items into it.
package Generic_Shared_Table is

  -- Types_Alstom declaration.
  -- -------------------------

  type Element_Index is new Types_Alstom.Natural_Long_T range 0 .. N_Max_Elements;
  type Element_Read_Access is access constant Element;
  type Element_Full_Access is access all Element;

  -- Getting current number of elements procedure.
  -- ---------------------------------------------

  -- Returns the current number of items currently defined in the shared table.
  --
  -- This procedure is intended to the reader, in order to implement a reading loop.
  function Current_Nb_Of_Elements return Element_Index;
  pragma Inline (Current_Nb_Of_Elements);

  -- Getting an element of the shared table.
  -- ---------------------------------------

  -- Returns an access (read only) on a given item of the shared table.
  -- The item is designed by its index, in range of currently defined items.
  -- In any error case, returns NULL access.
  --
  -- This procedure is intended to the reader, in order to get read access to a given element.
  function Read_Only_Element
   (Index : Element_Index)
    return  Element_Read_Access;
  -- No pragma inline allowed.

private

  -- Initialising procedure.
  -- -----------------------

  -- Initially the Table is empty but must be emptied at the beginning of each cycle.
  -- Clears the shared table, by resetting the internal number of defined items to ZERO.
  procedure Re_Init_Table;
  pragma Inline (Re_Init_Table);

  -- Writing an element into the shared table.
  -- -----------------------------------------

  -- Returns an access on next free item in shared table. If there is no free item, returns NULL access
  -- This procedure is reserved to privileged access to the only writer of the system.
  --
  -- It is exported only by the child package Generic_Shared_Table.Full_Access_Extension.
  function Next_Writable_Element return Element_Full_Access;
  -- No pragma inline allowed.

  -- Getting full access to an element of the shared table.
  -- ------------------------------------------------------

  -- This procedure is reserved to privileged access to the only writer of the system.
  -- This procedure is designed, in order to get privileged access to a given element.
  --
  -- It is exported only by the child package Generic_Shared_Table.Full_Access_Extension.
  function Writable_Element
   (Index : Element_Index)
    return  Element_Full_Access;

  -- Table is the shared buffer.
  -- As it is declared as private, no direct access to it is allowed.
  -- All access must be done through predefined services in the specification. (or through child test packages)
  type Table_T is array (Element_Index range 1 .. Element_Index'Last) of aliased Element;
  Table : Table_T;

  -- Index is the internal pointer on elements of the shared buffer.
  Max_Index_In_Use : Element_Index := 0;

end Generic_Shared_Table;
