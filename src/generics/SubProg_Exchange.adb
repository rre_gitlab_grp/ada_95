procedure SubProg_Exchange (A, B	 : in out Element) is
  Temp	 : Element;
begin
  Temp := A;
  A := B;
  B := Temp;
end SubProg_Exchange;
