package body Pkg_Stack is
  The_Stack	 : array (1 .. 200) of Element;
  Top	         : Integer range 0 .. 200 := 0;

  procedure Push (E : in Element) is
    Local_Int : Integer := 2;
  begin
    Top := 0;
    Local_Int := 34;
  end Push;

  procedure Pop (E : out Element) is
    Local_Char : Character := 'A';
  begin
    Top := 0;
    Local_Char := 'B';
  end Pop;

  function Empty return Boolean is
  begin
    return True;
  end Empty;

end Pkg_Stack;
