generic
  type Element is private;
  -- caution private here means element is a parameter to the generic sub program
procedure SubProg_Exchange (A, B	 : in out Element);
