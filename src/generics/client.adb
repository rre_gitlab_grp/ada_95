with Text_IO; use Text_IO;
with SubProg_Exchange;
with Pkg_Stack;
procedure Client  is
  -- instantiaon of Swap procedure for Integer
  procedure Swap_Int is new SubProg_Exchange (Element => Integer);  --elaboration: execution of declaration

  -- instantiaon of Swap procedure for Character
  procedure Swap_Char is new SubProg_Exchange (Element => Character);

  -- instantiation of stack package for integer
  package Stack_Int is new Pkg_Stack(Element => Integer);

  Var_Int_A : Integer := 10;
  Var_Int_B : Integer := 23;
begin

  Put_Line ("hello");
  Put_Line ("before swap value of A:" & Var_Int_B'Img);
  Swap_Int (Var_Int_A, Var_Int_B);
  Put_Line ("after swap value of A:" & Var_Int_B'Img);
  Swap_Int (Var_Int_A, Var_Int_B);
end Client;
