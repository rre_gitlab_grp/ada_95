generic
	type element is private;	-- note that this is a parameter to the generic
package Pkg_Stack is
	procedure push(e:	in element);
	procedure pop(e:	out element);
	function empty return boolean;
end Pkg_Stack;
